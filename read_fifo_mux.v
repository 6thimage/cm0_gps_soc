module read_fifo_mux (
    input clk,
    /* control */
    input mux_en,
    input mode,
    input [15:0] count,
    output is_counting,
    /* fifo */
    output reg read_en,
    input [7:0] read_data,
    input read_empty,
    /* A */
    input A_read_en,
    output reg [7:0] A_read_data,
    output reg A_read_empty,
    /* B */
    input B_read_en,
    output reg [7:0] B_read_data,
    output reg B_read_empty
    );

    localparam MODE_DUMB=0, MODE_COUNT=1;

    initial
    begin
        read_en = 1'b0;
        A_read_data = 'd0;
        A_read_empty = 1'b1;
        B_read_data = 'd0;
        B_read_empty = 1'b1;
    end

    /* mux en edge detection */
    reg last_mux_en=1'b0;
    always @(posedge clk) last_mux_en <= mux_en;
    wire mux_en_edge=(!last_mux_en && mux_en)?1'b1:1'b0;

    /* internal registers */
    reg mux_sel=1'b0;
    reg mode_int=1'b0;
    reg [15:0] count_int='d0;
    assign is_counting=((mode_int==MODE_COUNT) && (count_int!='d0))?1'b1:1'b0;

    always @(posedge clk)
    begin
        /* set the default as a normal mux */
        mux_sel <= mux_en;

        /* on the rising edge, store mode and count internally */
        if(mux_en_edge)
        begin
            mode_int <= mode;
            count_int <= count;
        end

        /* if we are in counting mode, decrement count on B read until zero
         * when we reach zero, return the mux to A
         */
        if(mode_int==MODE_COUNT)
        begin
            if(count_int!='d0)
            begin
                mux_sel <= 1'b1;

                if(B_read_en)
                    count_int <= count_int - 1'b1;
            end
            else
                mux_sel <= 1'b0;
        end
    end

    always @*
    begin
        A_read_empty = 1'b1;
        A_read_data = 'd0;
        B_read_empty = 1'b1;
        B_read_data = 'd0;
        read_en = 1'b0;

        if(mux_sel==1'b0)
        begin
            A_read_empty = read_empty;
            A_read_data = read_data;
            read_en = A_read_en;
        end
        else
        begin
            B_read_empty = read_empty;
            B_read_data = read_data;
            read_en = B_read_en;
        end
    end

endmodule
