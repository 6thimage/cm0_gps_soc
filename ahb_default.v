module ahb_default(
    input hclk,
    input hresetn,
    input hsel,
    input hready,
    input [1:0] htrans,
    output hreadyout,
    output hrespout
    );

    reg [1:0] resp_state=2'b01;
    assign hreadyout=resp_state[0];
    assign hrespout=resp_state[1];

    wire transfer_req=hready & hsel & htrans[1];

    always @(posedge hclk or negedge hresetn)
    begin
        if(!hresetn)
            resp_state <= 2'b01;
        else
            resp_state <= {transfer_req | (!resp_state[0]), !transfer_req};
    end

endmodule
