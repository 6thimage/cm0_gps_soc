#! /bin/bash

set -e

settings="settings.sh"

help_msg="Build tool for Xilinx\nUsage:\n\t./build.sh [synth][impl][bit][clean]\n"

# process arguments
if [ $# -eq 0 ]; then
    echo -e 'Error - no arguments passed\n'
    echo -en $help_msg
    exit
elif [ $# -gt 1 ]; then
    echo -e 'Error - too many arguments\n'
    echo -en $help_msg
    exit
fi

case $1 in
    synth)
        mode=synth
        ;;
    impl)
        mode=impl
        ;;
    bit)
        mode=bit
        ;;
    clean)
        mode=clean
        ;;
    *)
        echo -e "Error - unknown command ($1)\n"
        echo -en $help_msg
        exit
        ;;
esac
shift

# source settings
if [ ! -e $settings ]; then
    echo "Error - settings file not at ($settings)"
    exit
fi
. $settings

# check environment
if [ ! -e $xilinx_settings ]; then
    echo "Error - Xilinx settings file not at ($xilinx_settings)"
    exit
fi

# save old library path
ld_path=$LD_LIBRARY_PATH
# source xilinx settings / environment
. $xilinx_settings &> /dev/null
# save xilinx library path
xilinx_ld_path=$LD_LIBRARY_PATH
# reset library path
export LD_LIBRARY_PATH=$ld_path

# this function sets the library path to the xilinx version just while the xilinx program is running
# this allows the output to be pipped to awk, which otherwise won't work due to different library
# versions - if only xilinx provided dependencies, rather than this nasty hack
xld()
{
    export LD_LIBRARY_PATH=$xilinx_ld_path
    $*
    export LD_LIBRARY_PATH=$ld_path
}

if [ $mode = synth ]; then
    if [ ! -d xst/projnav.tmp ]; then
        mkdir -p xst/projnav.tmp
    fi

    # perform the synthesis and reduce the printed output
    xld /usr/bin/time xst -intstyle ise -ifn ${output_name}.xst -ofn ${output_name}.syr |awk 'BEGIN{p=0}{s=substr($0,0,1); if(s=="*") print $0; if($1=="HDL" || $1=="Advanced" || $1=="Final" || $1=="Partition" || $1=="Clock" || $1=="Device") p=1; else if(s=="=") p=0; if(p || s=="=" || s=="-" || $2=="|" || $1=="Reading" || $1=="Parsing" || $1=="Elaborating" || $1=="Synthesizing" || $1=="Optimizing") print $0;}'

    # get device utilisation summary from output file - it is not part of xst's printed output
    echo
    echo '---------------------------'
    awk 'BEGIN{p=0}{if($1=="Device") p=2; else if(substr($0,0,1)=="-" && p) --p; if(p) print $0;}' ${output_name}.syr

    # remove unnecessary files
    [ -e ${output_name}.lso ] && rm ${output_name}.lso
    [ -e ${output_name}.ngr ] && rm ${output_name}.ngr # this is the xilinx RTL schematic file
    [ -e ${output_name}_xst.xrpt ] && rm ${output_name}_xst.xrpt
    [ -d xst ] && rm -r xst
    [ -d _xmsgs ] && rm -r _xmsgs

    # the only additional files at this point are
    # ${output_name}.ngc - synth netlist
    # ${output_name}.syr - synth report
fi

if [ $mode = impl ]; then
    if [ ! -e ${output_name}.ngc ]; then
        echo "Unable to find ${output_name}.ngc, please run synth first"
        exit
    fi

    xld ngdbuild -intstyle ise -dd _ngo -nt timestamp ${ngdbuild_search_dir} -uc ${constraints} -p ${part} ${output_name}.ngc ${output_name}.ngd | cat -s

    # remove unnecessary files
    [ -e ${output_name}_ngdbuild.xrpt ] && rm ${output_name}_ngdbuild.xrpt
    [ -d _ngo ] && rm -r _ngo
    [ -d xlnx_auto_0_xdb ] && rm -r xlnx_auto_0_xdb
    [ -d _xmsgs ] && rm -r _xmsgs

    # additional files
    # ${output_name}.bld - build log
    # ${output_name}.ngd - build netlist

    xld map -intstyle ise -w -p ${part} ${map_options} -o ${output_name}_map.ncd ${output_name}.ngd ${output_name}.pcf | cat -s

    # remove unnecessary files
    [ -e ${output_name}_map.ngm ] && rm ${output_name}_map.ngm
    [ -e ${output_name}_map.xrpt ] && rm ${output_name}_map.xrpt
    [ -e ${output_name}_summary.xml ] && rm ${output_name}_summary.xml
    [ -e ${output_name}_usage.xml ] && rm ${output_name}_usage.xml
    [ -d _xmsgs ] && rm -r _xmsgs

    # additional files
    # ${output_name}.pcf - physical constraints file
    # ${output_name}_map.map - map log
    # ${output_name}_map.mrp - map report
    # ${output_name}_map.ncd - map netlist

    xld par -w -intstyle ise ${par_options} ${output_name}_map.ncd ${output_name}.ncd ${output_name}.pcf | cat -s

    # remove unnecessary files
    [ -e ${output_name}.ptwx ] && rm ${output_name}.ptwx
    [ -e ${output_name}.xpi ] && rm ${output_name}.xpi
    [ -e ${output_name}_par.xrpt ] && rm ${output_name}_par.xrpt
    [ -e par_usage_statistics.html ] && rm par_usage_statistics.html
    [ -d _xmsgs ] && rm -r _xmsgs

    # do not keep spreadsheet importable versions of the pad report
    [ -e ${output_name}.pad ] && rm ${output_name}.pad
    [ -e ${output_name}_pad.csv ] && rm ${output_name}_pad.csv

    # additional files
    # ${output_name}.ncd - par netlist
    # ${output_name}.par - par report
    # ${output_name}.unroutes - unrouted signals
    # ${output_name}_pad.txt - pad report

    xld trce -intstyle ise ${trce_options} ${output_name}.ncd -o ${output_name}.twr ${output_name}.pcf

    # remove unnecessary files
    [ -e ${output_name}.twx ] && rm ${output_name}.twx
    [ -d _xmsgs ] && rm -r _xmsgs

    # additional files
    # ${output_name}.twr - trace report
fi

if [ $mode = bit ]; then
    if [ ! -e ${output_name}.ncd ]; then
        echo "Unable to find ${output_name}.ncd, please run impl first"
        exit
    fi

    xld bitgen -intstyle ise -w ${bitgen_options} ${output_name}.ncd 

    # remove unnecessary files
    [ -e ${output_name}_bitgen.xwbt ] && rm ${output_name}_bitgen.xwbt
    [ -e ${output_name}_summary.xml ] && rm ${output_name}_summary.xml
    [ -e ${output_name}_usage.xml ] && rm ${output_name}_usage.xml
    [ -e usage_statistics_webtalk.html ] && rm usage_statistics_webtalk.html
    [ -e webtalk.log ] && rm webtalk.log
    [ -d _xmsgs ] && rm -r _xmsgs

    [ -e ${output_name}.drc ] && rm ${output_name}.drc # design rule check - should also be in report

    # additional files
    # ${output_name}.bgn - bitget report
    # ${output_name}.bit - bitstream file
fi

if [ $mode = clean ]; then
    folders='xlnx_auto_0_xdb xst _ngo _xmsgs'
    files='*.bgn *.bit *.bld *.csv *.drc *.html *.log *.lso *.map *.mrp *.ncd *.ngc *.ngr *.ngd *.ngm *.pad *.par *.pcf *.ptwx *.unroutes *.syr *.txt *.twr *.xml *.xpi *.xrpt *.xwbt'
    for f in $folders
    do
        [ -d $f ] && rm -r $f
    done
    for f in $files
    do
        [ -e $f ] && rm $f
    done

    exit 0
fi
