#! /bin/bash

set -e

settings="settings.sh"

# soruce settings
if [ ! -e $settings ]; then
    echo "Error - settings file not at ($settings)"
    exit
fi
. $settings

# check environment
if [ ! -e $xilinx_settings ]; then
    echo "Error - Xilinx settings file not at ($xilinx_settings)"
    exit
fi

# save old library path
ld_path=$LD_LIBRARY_PATH
# source xilinx settings / environment
. $xilinx_settings &> /dev/null
# save xilinx library path
xilinx_ld_path=$LD_LIBRARY_PATH
# reset library path
export LD_LIBRARY_PATH=$ld_path

# this function sets the library path to the xilinx version just while the xilinx program is running
# this allows the output to be pipped to awk, which otherwise won't work due to different library
# versions - if only xilinx provided dependencies, rather than this nasty hack
xld()
{
    export LD_LIBRARY_PATH=$xilinx_ld_path
    $*
    export LD_LIBRARY_PATH=$ld_path
}

xld xdl -ncd2xdl ${output_name}.ncd .temp.xdl &>/dev/null

#grep 'inst' .temp.xdl|grep '"ram/M'
./bmm_gen.py .temp.xdl ram 32 > ram.bmm

rm .temp.xdl
rm -r _xmsgs
