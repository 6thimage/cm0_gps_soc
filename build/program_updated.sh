#! /bin/bash

set -e

settings="settings.sh"

help_msg="Programming tool for Pipistrello\nUage:\n\t./program.sh [][flash]\n"

# process arguments
if [ $# -gt 1 ]; then
    echo -e "Error - too many arguments\n"
    echo -en $help_msg
    exit
fi

if [ $# -eq 0 ]; then
    flash=0
elif [ $1 == flash ]; then
    flash=1
else
    echo -e "Error - unrecognised argument\n"
    echo -en $help_msg
    exit
fi

# soruce settings
if [ ! -e $settings ]; then
    echo "Error - settings file not at ($settings)"
    exit
fi
. $settings

# run fpgaprog
if [ $flash -eq 0 ]; then
    fpgaprog -v -f ${output_name}_updated.bit
else
    fpgaprog -v -f ${output_name}_updated.bit -b "${fpga_bscan}" -sa -r
fi
