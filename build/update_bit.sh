#! /bin/bash

if [ -e cm0_gps_soc_updated.bit ];
then
    rm cm0_gps_soc_updated.bit
fi

/opt/Xilinx/14.7/ISE_DS/ISE/bin/lin64/data2mem -bm ram.bmm -bd ../software/image.mem -bt cm0_gps_soc.bit -o b cm0_gps_soc_updated.bit
