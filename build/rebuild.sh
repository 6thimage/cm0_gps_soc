#! /bin/bash
set -e

msg=""

./build.sh clean

SECONDS=0
./build.sh synth
msg+="Synth: $(($SECONDS / 60)) m $(($SECONDS % 60)) s\n"

SECONDS=0
./build.sh impl
msg+="Impl: $(($SECONDS / 60)) m $(($SECONDS % 60)) s\n"

SECONDS=0
./build.sh bit
msg+="Bit: $(($SECONDS / 60)) m $(($SECONDS % 60)) s"

notify-send -u critical "Build complete" "$msg"
