#! /usr/bin/env python3

import argparse
import re

parser=argparse.ArgumentParser()
parser.add_argument('xdl', help='file name of the generated xdl file', type=str)
parser.add_argument('name', help='name of the BRAM entities to find', type=str)
parser.add_argument('width', help='width of the memory', type=int)

args=parser.parse_args()

width=args.width
bram_name='inst "{}'.format(args.name).lower()

# scan through file extracting any brams
brams=[]
with open(args.xdl, 'r') as f:
    for line in f:
        lower_line=line.lower()
        if 'bramsite' in lower_line and bram_name in lower_line:
            brams.append(line.rstrip('\n'))

if len(brams)==0:
    print("Failed to find BRAMS")
    exit()

# reduce to coordinates
for i in range(0, len(brams)):
    line=brams[i]
    line=line.split(' ')
    # extract name and coordinate
    name=line[1].strip('"')
    coord=line[4]
    # generate number from name
    num_start=re.search('\d', name)
    if not num_start:
        continue
    number=name[num_start.start():]
    brams[i]=[number, name, coord]

# sort brams according to number
brams=sorted(brams, key=lambda x:x[0])

# get name of entity
memory_name=brams[0][1]
left=re.search('/', memory_name)
right=re.search('\d', memory_name)
if not left or not right:
    print('Failed to extract name ({}, {})'.format(left, right))
    exit()
memory_name=memory_name[left.end():right.start()]

# find size of bram
bram_size=brams[0][2]
left=re.search('\d', bram_size)
right=re.search('_', bram_size)
if not left or not right:
    print('Failed to extract size ({}, {})'.format(left, right))
    exit()
bram_name=bram_size[:right.start()]
bram_size=bram_name[left.start():]
bram_size=int(bram_size)*512

print('ADDRESS_SPACE {} COMBINED [0x00:0x{:X}]'.format(memory_name, bram_size-1))
tab=' '*4

for bram in brams:
    left=re.search('_', bram[2])
    if not left:
        print('Failed to extract site ({})'.format(bram))
        exit()
    site=bram[2][left.start()+1:]
    print(tab+'ADDRESS_RANGE {}'.format(bram_name))
    print(2*tab+'BUS_BLOCK')
    print(3*tab+'{} [{}:0] PLACED = {};'.format(bram[1], width-1, site))
    print(2*tab+'END_BUS_BLOCK;')
    print(tab+'END_ADDRESS_RANGE;')

print('END_ADDRESS_SPACE;')
