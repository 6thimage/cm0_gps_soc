module ahb_channel_group #(
    parameter num_channels=1
    )
    (
    input hclk,
    input hresetn,
    /* decode line */
    input hsel,
    /* inputs */
    input hready,
    input [31:0] haddr,
    input [1:0] htrans,
    input hwrite,
    input [2:0] hsize,
    input [31:0] hwdata,
    /* outputs */
    output hreadyout,
    output reg [31:0] hrdata,
    /* interrupt */
    output interrupt,
    /* from baseband */
    input rf_data_valid,
    /* channels */
    output reg baseband_mode,
    output reg signed [15:0] carrier_c1, carrier_c2,
    output reg signed [25:0] code_c1, code_c2,
    input [num_channels-1:0] channel_active,
    input [num_channels-1:0] frame_ready,
    input [(17*num_channels)-1:0] TOW,
    input [(10*num_channels)-1:0] IODC,
    input [num_channels-1:0] IOD_match,
    output reg [num_channels-1:0] code_num_req,
    input [(46*num_channels)-1:0] code_num
    );

/* address map
 *              register
 * base +0x0    control
 * base +0x4    carrier constants
 * base +0x8    code constant 1
 * base +0xc    code constant 2
 * base +0x10   interrupt enable
 * base +0x14   interrupt flags
 * base +0x18   valid channels
 *
 * channel_base(n)  (n*0x20)+0x20   -- channels are numbered from zero
 * cbase +0x0   TOW
 * cbase +0x4   IOD
 * cbase +0x8   code num top
 * cbase +0xc   code num bottom
 * cbase +0x10  channel time
 *
 * register map
 *                   |33222222|22221111|11111100|00000000|
 *                   |10987654|32109876|54321098|76543210|
 * control           |********|********|********|******BR|
 * carrier constants |<  carrier c2   >|<  carrier c1   >|
 * code constant 1   |******<          code c1          >|
 * code constant 2   |******<          code c2          >|
 * interrupt enable  |PPPPPPPP|PPPPPPPP|PPPPPPPP|PPPPPPPP|
 * interrupt flags   |PPPPPPPP|PPPPPPPP|PPPPPPPP|PPPPPPPP|
 * valid channels    |PPPPPPPP|PPPPPPPP|PPPPPPPP|PPPPPPPP|
 * channel #n
 *   TOW             |********|*******<       TOW       >|
 *   IOD             |********|********|******<   IOD   >|
 *   code num top    |<         code num[45:14]         >|
 *   code num bottom |********|********|**< c num[13:0] >|
 *   channel time    |****<        channel time         >|
 *
 * control
 *      0       code number request
 *      1       baseband mode
 *      2..31   unused
 * carrier constants
 *      0..15   carrier c1
 *      16..31  carrier c2
 * code constant 1
 *      0..25   code c1
 *      26..31  unused
 * code constant 2
 *      0..25   code c2
 *      26..31  unused
 * interrupt enable
 *      0..31   parameterised
 * interrupt flags
 *      0..31   parameterised
 * valid channels
 *      0..31   parameterised
 *
 * channel #n
 * TOW
 *      0..16   TOW
 *      17..31  unused
 * IOD
 *      0..9    IODC
 *      10..31  unused
 * code num top
 *      0..31   code num[45:14]
 * code num bottom
 *      0..13   code num[13:0]
 *      14..31  unused
 * channel time
 *      0..26   channel time
 *      27..31  unused
 */
localparam control_addr=6'd0,
           carrier_const_addr=6'd1,
           code_const1_addr=6'd2,
           code_const2_addr=6'd3,
           interrupt_enable_addr=6'd4,
           interrupt_flags_addr=6'd5,
           valid_channels_addr=6'd6;

assign hreadyout=1'b1;

/* address phase registers */
reg addr_phase_hsel=1'b0;
reg [31:0] addr_phase_haddr;
reg [1:0] addr_phase_htrans;
reg addr_phase_hwrite;
reg [2:0] addr_phase_hsize;

/* capture address phase */
always @(posedge hclk or negedge hresetn)
begin
    if(!hresetn)
    begin
        addr_phase_hsel <= 1'b0;
        addr_phase_haddr <= 32'd0;
        addr_phase_htrans <= 2'd0;
        addr_phase_hwrite <= 1'b0;
        addr_phase_hsize <= 3'd0;
    end
    else if(hready)
    begin
        addr_phase_hsel <= hsel;
        addr_phase_haddr <= haddr;
        addr_phase_htrans <= htrans;
        addr_phase_hwrite <= hwrite;
        addr_phase_hsize <= hsize;
    end
end

/* transaction size decode */
wire tx_byte=addr_phase_hsize[1:0]==2'b00;
wire tx_half=addr_phase_hsize[1:0]==2'b01;
wire tx_word=addr_phase_hsize[1:0]==2'b10;

wire byte_at_0=tx_byte & (addr_phase_haddr[1:0]==2'd0);
wire byte_at_1=tx_byte & (addr_phase_haddr[1:0]==2'd1);
wire byte_at_2=tx_byte & (addr_phase_haddr[1:0]==2'd2);
wire byte_at_3=tx_byte & (addr_phase_haddr[1:0]==2'd3);

wire half_at_0=tx_half & (addr_phase_haddr[1:0]==2'd0);
wire half_at_1=tx_half & (addr_phase_haddr[1:0]==2'd2);

wire word_at_0=tx_word & (addr_phase_haddr[1:0]==2'd0);

wire byte0=word_at_0 | half_at_0 | byte_at_0;
wire byte1=word_at_0 | half_at_0 | byte_at_1;
wire byte2=word_at_0 | half_at_1 | byte_at_2;
wire byte3=word_at_0 | half_at_1 | byte_at_3;

/* interrupts */
reg [num_channels-1:0] interrupt_flags_frame_ready='d0;
wire [31:0] interrupt_flags={{(32-num_channels){1'b0}}, interrupt_flags_frame_ready};

reg [num_channels-1:0] interrupt_enable_frame_ready='d0;
wire [31:0] interrupt_enable={{(32-num_channels){1'b0}}, interrupt_enable_frame_ready};

assign interrupt=((interrupt_enable&interrupt_flags)!='d0);

/* channel timers
 * these are counters that increment when rf_data_valid is high
 * and reset when a channel's frame is ready
 *
 * the spacing between frames is 6 seconds (300 bits and 50 bps)
 * at the baseband frequency (16.368 MHz), there are 98,208,000 samples per frame
 * a 27 bit counter is the smallest that can store this (27 bits is up to 134,217,727)
 */
reg [26:0] channel_timer[num_channels-1:0];
integer i;
always @(posedge hclk)
begin
    for(i=0; i<num_channels; i=i+1)
    begin
        if(channel_active[i])
        begin
            if(rf_data_valid)
                channel_timer[i] <= channel_timer[i] + 1'b1;

            if(frame_ready[i])
                channel_timer[i] <= 'd0;
        end
    end
end

/* presistence registers */
reg [(17*num_channels)-1:0] TOW_pers;
reg [(10*num_channels)-1:0] IODC_pers;
reg [26:0] channel_timer_pers[num_channels-1:0];

reg [num_channels-1:0] valid_channels='d0;

/* write */
always @(posedge hclk or negedge hresetn)
begin
    if(!hresetn)
    begin
        code_num_req <= 'd0;
        interrupt_flags_frame_ready <= 'd0;
        valid_channels <= 'd0;
    end
    else
    begin
        /* default outputs */
        code_num_req <= 'd0;

        /* latching interrupt flags */
        interrupt_flags_frame_ready <= interrupt_flags_frame_ready | frame_ready;

        /* results for code number request */
        if(code_num_req)
        begin
            /* mark valid channels as those that have matching IODs */
            valid_channels <= code_num_req & IOD_match;

            /* copy requested channel information into local registers (for persistence) */
            TOW_pers <= TOW;
            IODC_pers <= IODC;
            for(i=0; i<num_channels; i=i+1)
                channel_timer_pers[i] <= channel_timer[i];
        end

        if(addr_phase_hsel & addr_phase_hwrite & addr_phase_htrans[1])
        begin
            case(addr_phase_haddr[7:2])
                control_addr:
                begin
                    if(byte0)
                    begin
                        if(hwdata[0])
                            /* send code number request to active channels */
                            code_num_req <= channel_active;
                        baseband_mode <= hwdata[1];
                    end
                end
                carrier_const_addr:
                begin
                    if(byte0)
                        carrier_c1[7:0] <= hwdata[7:0];
                    if(byte1)
                        carrier_c1[15:8] <= hwdata[15:8];
                    if(byte2)
                        carrier_c2[7:0] <= hwdata[23:16];
                    if(byte3)
                        carrier_c2[15:8] <= hwdata[31:24];
                end
                code_const1_addr:
                begin
                    if(byte0)
                        code_c1[7:0] <= hwdata[7:0];
                    if(byte1)
                        code_c1[15:8] <= hwdata[15:8];
                    if(byte2)
                        code_c1[23:16] <= hwdata[23:16];
                    if(byte3)
                        code_c1[25:24] <= hwdata[25:24];
                end
                code_const2_addr:
                begin
                    if(byte0)
                        code_c2[7:0] <= hwdata[7:0];
                    if(byte1)
                        code_c2[15:8] <= hwdata[15:8];
                    if(byte2)
                        code_c2[23:16] <= hwdata[23:16];
                    if(byte3)
                        code_c2[25:24] <= hwdata[25:24];
                end
                interrupt_enable_addr:
                begin
                    if(byte0)
                        if(num_channels>8)
                            interrupt_enable_frame_ready[7:0] <= hwdata[7:0];
                        else
                            interrupt_enable_frame_ready[num_channels-1:0] <= hwdata[num_channels-1:0];
                    if(byte1 & (num_channels>8))
                        if(num_channels>16)
                            interrupt_enable_frame_ready[15:8] <= hwdata[15:8];
                        else
                            interrupt_enable_frame_ready[num_channels-9:8] <= hwdata[num_channels-9:8];
                    if(byte2 & (num_channels>16))
                        if(num_channels>24)
                            interrupt_enable_frame_ready[24:16] <= hwdata[24:16];
                        else
                            interrupt_enable_frame_ready[num_channels-17:16] <= hwdata[num_channels-17:16];
                    if(byte3 & (num_channels>24))
                        interrupt_enable_frame_ready[num_channels-19:18] <= hwdata[num_channels-19:18];
                end
                interrupt_flags_addr:
                begin
                    if(byte0)
                        if(num_channels>8)
                            interrupt_flags_frame_ready[7:0] <= hwdata[7:0];
                        else
                            interrupt_flags_frame_ready[num_channels-1:0] <= hwdata[num_channels-1:0];
                    if(byte1 & (num_channels>8))
                        if(num_channels>16)
                            interrupt_flags_frame_ready[15:8] <= hwdata[15:8];
                        else
                            interrupt_flags_frame_ready[num_channels-9:8] <= hwdata[num_channels-9:8];
                    if(byte2 & (num_channels>16))
                        if(num_channels>24)
                            interrupt_flags_frame_ready[24:16] <= hwdata[24:16];
                        else
                            interrupt_flags_frame_ready[num_channels-17:16] <= hwdata[num_channels-17:16];
                    if(byte3 & (num_channels>24))
                        interrupt_flags_frame_ready[num_channels-19:18] <= hwdata[num_channels-19:18];
                end
            endcase
        end
    end
end

/* recreate separate wires for read back */
wire [16:0] TOW_pers_split[0:num_channels-1];
wire [9:0] IODC_pers_split[0:num_channels-1];
wire [31:0] code_num_split_top[0:num_channels-1];
wire [13:0] code_num_split_btm[0:num_channels-1];
genvar j;
generate
    for(j=0; j<num_channels; j=j+1)
    begin :channel_group_split
        assign TOW_pers_split[j] = TOW_pers[((j+1)*17)-1:j*17];
        assign IODC_pers_split[j] = IODC_pers[((j+1)*10)-1:j*10];
        assign code_num_split_top[j] = code_num[((j+1)*46)-1:(j*46)+14];
        assign code_num_split_btm[j] = code_num[((j+1)*46)-33:j*46];
    end
endgenerate

/* read */
always @(posedge hclk)
begin
    if(hready & hsel & (!hwrite) & htrans[1])
    begin
        case(haddr[8:2])
            control_addr: hrdata <= {30'd0, baseband_mode, 1'd0};
            carrier_const_addr: hrdata <= {carrier_c2, carrier_c1};
            code_const1_addr: hrdata <= {6'd0, code_c1};
            code_const2_addr: hrdata <= {6'd0, code_c2};
            interrupt_enable_addr: hrdata <= interrupt_enable;
            interrupt_flags_addr: hrdata <= interrupt_flags;
            valid_channels_addr: hrdata <= {{(32-num_channels){1'b0}}, valid_channels};
            default:
            begin
                hrdata <= 'd0;

                for(i=0; i<num_channels; i=i+1)
                begin
                    /* channel_base(n) >> 2 is equivalent to (n*4)+4 */
                    /* TOW */
                    if(haddr[8:2]==((i*8)+8))
                        hrdata <= {15'd0, TOW_pers_split[i]};
                    /* IODC */
                    if(haddr[8:2]==(((i*8)+8)+1))
                        hrdata <= {22'd0, IODC_pers_split[i]};
                    /* code_num[45:14] */
                    if(haddr[8:2]==(((i*8)+8)+2))
                        hrdata <= code_num_split_top[i];
                    /* code_num[13:0] */
                    if(haddr[8:2]==(((i*8)+8)+3))
                        hrdata <= {18'd0, code_num_split_btm[i]};
                    /* channel time */
                    if(haddr[8:2]==(((i*8)+8)+4))
                        hrdata <= {5'd0, channel_timer_pers[i]};
                end
            end
        endcase
    end
end

endmodule

