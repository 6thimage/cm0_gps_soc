module ahb_channel(
    input hclk,
    input hresetn,
    /* decode line */
    input hsel,
    /* inputs */
    input hready,
    input [31:0] haddr,
    input [1:0] htrans,
    input hwrite,
    input [2:0] hsize,
    input [31:0] hwdata,
    /* outputs */
    output hreadyout,
    output reg [31:0] hrdata,
    /* baseband */
    input rf_data_valid,
    input [1:0] I, Q,
    /* to channel group */
    input baseband_mode,
    input signed [15:0] carrier_c1, carrier_c2,
    input signed [25:0] code_c1, code_c2,
    output channel_active,
    output frame_ready,
    output [16:0] TOW,
    output [9:0] IODC,
    output IOD_match,
    input code_num_req,
    output [45:0] code_num
    );

    /* address map
     *              register
     * base +0x0    status
     * base +0x4    correlator
     * base +0x8    carrier NCO 1
     * base +0xc    carrier NCO 2
     * base +0x10   code NCO 1
     * base +0x14   code NCO 2
     * base +0x18   decoder
     * base +0x1c   subframe 1a
     * base +0x20   subframe 1b
     * base +0x24   subframe 1c
     * base +0x28   subframe 1d
     * base +0x2c   subframe 2a
     * base +0x30   subframe 2b
     * base +0x34   subframe 2c
     * base +0x38   subframe 2d
     * base +0x3c   subframe 2e
     * base +0x40   subframe 2f
     * base +0x44   subframe 3a
     * base +0x48   subframe 3b
     * base +0x4c   subframe 3c
     * base +0x50   subframe 3d
     * base +0x54   subframe 3e
     * base +0x58   subframe 3f
     * base +0x5c   subframe 3g
     *
     * register map
     *                   |33222222|22221111|11111100|00000000|
     *                   |10987654|32109876|54321098|76543210|
     * status            |********|********|********|***POCRA|
     * correlator        |********|********|********|***<PRN>|
     * carrier NCO 1     |**<         carrier theta         >|
     * carrier NCO 2     |**<         carrier step          >|
     * code NCO 1        |********|********|**<CS><code num >|
     * code NCO 2        |<         code step[31:0]         >|
     * decoder           |<       TOW       >*******|**IA<V>R|
     * subframe 1a       |******<week num >|<SVH>B<  IODC   >|
     * subframe 1b       |****<UI>|< T_GD >|<      t_oc     >|
     * subframe 1c       |********|**<         a_f0         >|
     * subframe 1d       |********|< a_f2 >|<     a_f1      >|
     * subframe 2a       |<    Delta_n    >|*******C|<IODE_2>|
     * subframe 2b       |<               M_0               >|
     * subframe 2c       |<               ecc               >|
     * subframe 2d       |<             sqrt_A              >|
     * subframe 2e       |<     t_oe      >|<     C_rs      >|
     * subframe 2f       |<     C_uc      >|<     C_us      >|
     * subframe 3a       |**<    IDOT     >|********|<IODE_3>|
     * subframe 3b       |<             Omega_0             >|
     * subframe 3c       |<             incl_0              >|
     * subframe 3d       |<              omega              >|
     * subframe 3e       |********|<       Omega_dot        >|
     * subframe 3f       |********|********|<     C_rc      >|
     * subframe 3g       |<     C_ic      >|<     C_is      >|
     *
     * status
     *      0       active
     *      1       correlator running
     *      2       correlator configure
     *      3       optimistic lock
     *      4       pessimistic lock
     *      5..31   unused
     * correlator
     *      0..4    PRN
     *      5..31   unsued
     * carrier NCO 1
     *      0..29   carrier theta
     *      30..31  unused
     * carrier NCO 2
     *      0..29   carrier step
     *      30..31  unused
     * code NCO 1
     *      0..9    code number
     *      10..13  code step[35:32]
     *      14..31  unused
     * code NCO 2
     *      0..31   code step[31:0]
     * decoder
     *      0       frame ready
     *      1..3    frame valid
     *      4       alert flag
     *      5       enhanced integrity flag
     *      6..14   unused
     *      15..31  TOW (time of week)
     * subframe 1a
     *      0..9    IODC
     *      10      SV health bad
     *      11..15  SV health
     *      16..25  week number
     *      26..31  unused
     * subframe 1b
     *      0..15   t_oc
     *      16..23  T_GD
     *      24..27  URA index
     *      28..31  unused
     * subframe 1c
     *      0..21   a_f0
     *      22..31  unused
     * subframe 1d
     *      0..15   a_f1
     *      16..23  a_f2
     *      24..31  unused
     * subframe 2a
     *      0..7    IODE_sf2
     *      8       curve fit greater
     *      9..15   unused
     *      16..31  Delta_n
     * subframe 2b
     *      0..31   M_0
     * subframe 2c
     *      0..31   ecc
     * subframe 2d
     *      0..31   sqrt_A
     * subframe 2e
     *      0..15   C_rs
     *      16..31  t_oe
     * subframe 2f
     *      0..15   C_uc
     *      16..31  C_us
     * subframe 3a
     *      0..7    IODE_sf3
     *      8..15   unused
     *      16..29  IDOT
     *      30..31  unused
     * subframe 3b
     *      0..31   Omega_0
     * subframe 3c
     *      0..31   incl_0
     * subframe 3d
     *      0..31   omega
     * subframe 3e
     *      0..23   Omega_dot
     *      24..31  unused
     * subframe 3f
     *      0..15   C_rc
     *      16..31  unused
     * subframe 3g
     *      0..15   C_is
     *      16..31  C_ic
     */
    localparam status_addr=6'd0,
               correlator_addr=6'd1,
               carrier_nco1_addr=6'd2,
               carrier_nco2_addr=6'd3,
               code_nco1_addr=6'd4,
               code_nco2_addr=6'd5,
               decoder_addr=6'd6,
               subframe_1a_addr=6'd7,
               subframe_1b_addr=6'd8,
               subframe_1c_addr=6'd9,
               subframe_1d_addr=6'd10,
               subframe_2a_addr=6'd11,
               subframe_2b_addr=6'd12,
               subframe_2c_addr=6'd13,
               subframe_2d_addr=6'd14,
               subframe_2e_addr=6'd15,
               subframe_2f_addr=6'd16,
               subframe_3a_addr=6'd17,
               subframe_3b_addr=6'd18,
               subframe_3c_addr=6'd19,
               subframe_3d_addr=6'd20,
               subframe_3e_addr=6'd21,
               subframe_3f_addr=6'd22,
               subframe_3g_addr=6'd23;

    assign hreadyout=1'b1;

    /* correlator */
    reg [4:0] prn;
    reg configure;
    reg [29:0] conf_carrier_theta, conf_carrier_step;
    reg [9:0] conf_code_number;
    reg [35:0] conf_code_step;
    wire correlator_running;

    reg channel_reset=1'b0;
    assign channel_active=channel_reset;
    wire code_sign, code_sign_valid;
    wire optimistic_lock, pessimistic_lock;
    correlator ch_correlator(.clk(hclk), .reset(channel_reset),
                             .rf_data_valid(rf_data_valid), .I(I), .Q(Q),
                             .prn(prn), .baseband_mode(baseband_mode), .carrier_c1(carrier_c1),
                                 .carrier_c2(carrier_c2), .code_c1(code_c1), .code_c2(code_c2),
                             .configure(configure), .conf_carrier_theta(conf_carrier_theta),
                                 .conf_carrier_step(conf_carrier_step),
                                 .conf_code_number(conf_code_number), .conf_code_step(conf_code_step),
                             .running(correlator_running), .code_sign(code_sign),
                                 .code_sign_valid(code_sign_valid), .code_num_req(code_num_req),
                                 .code_num(code_num), .optimistic_lock(optimistic_lock),
                                 .pessimistic_lock(pessimistic_lock));

    /* decoder */
    wire [2:0] frame_valid;
    wire alert_flag;
    wire enhanced_integrity;
    /* */
    wire [9:0] week_number;
    wire [3:0] URA_index;
    wire SV_health_bad;
    wire [4:0] SV_health;
    wire [15:0] t_oc;
    wire signed [7:0] T_GD;
    wire signed [7:0] a_f2;
    wire signed [15:0] a_f1;
    wire signed [21:0] a_f0;
    /* */
    wire [7:0] IODE_sf2;
    wire curve_fit_greater;
    wire signed [31:0] M_0, ecc;
    wire [31:0] sqrt_A;
    wire signed [15:0] Delta_n, t_oe, C_rs, C_uc, C_us;
    /* */
    wire [7:0] IODE_sf3;
    wire signed [31:0] Omega_0, incl_0, omega;
    wire signed [23:0] Omega_dot;
    wire signed [15:0] C_rc, C_ic, C_is;
    wire signed [13:0] IDOT;

    assign IOD_match=((IODC[7:0]==IODE_sf2)&&(IODE_sf2==IODE_sf3))?1'b1:1'b0;

    decoder ch_decoder(.clk(hclk), .reset(channel_reset),
                       .code_sign_pos(code_sign), .code_sign_valid(code_sign_valid),
                       .frame_ready(frame_ready), .frame_valid(frame_valid), .preamble_tick(),
                       .TOW(TOW), .alert_flag(alert_flag), .enhanced_integrity(enhanced_integrity),
                       .week_number(week_number), .URA_index(URA_index), .SV_health_bad(SV_health_bad),
                           .SV_health(SV_health), .IODC(IODC), .t_oc(t_oc), .T_GD(T_GD), .a_f2(a_f2),
                           .a_f1(a_f1), .a_f0(a_f0),
                       .IODE_sf2(IODE_sf2), .curve_fit_greater(curve_fit_greater), .M_0(M_0),
                           .ecc(ecc), .sqrt_A(sqrt_A), .Delta_n(Delta_n), .t_oe(t_oe), .C_rs(C_rs),
                           .C_uc(C_uc),.C_us(C_us),
                       .IODE_sf3(IODE_sf3), .Omega_0(Omega_0), .incl_0(incl_0), .omega(omega),
                           .Omega_dot(Omega_dot), .C_rc(C_rc), .C_ic(C_ic), .C_is(C_is), .IDOT(IDOT));


    /* address phase registers */
    reg addr_phase_hsel;
    reg [31:0] addr_phase_haddr;
    reg [1:0] addr_phase_htrans;
    reg addr_phase_hwrite;
    reg [2:0] addr_phase_hsize;

    /* capture address phase */
    always @(posedge hclk or negedge hresetn)
    begin
        if(!hresetn)
        begin
            addr_phase_hsel <= 1'b0;
            addr_phase_haddr <= 32'd0;
            addr_phase_htrans <= 2'd0;
            addr_phase_hwrite <= 1'b0;
            addr_phase_hsize <= 3'd0;
        end
        else if(hready)
        begin
            addr_phase_hsel <= hsel;
            addr_phase_haddr <= haddr;
            addr_phase_htrans <= htrans;
            addr_phase_hwrite <= hwrite;
            addr_phase_hsize <= hsize;
        end
    end

    /* transaction size decode */
    wire tx_byte=addr_phase_hsize[1:0]==2'b00;
    wire tx_half=addr_phase_hsize[1:0]==2'b01;
    wire tx_word=addr_phase_hsize[1:0]==2'b10;

    wire byte_at_0=tx_byte & (addr_phase_haddr[1:0]==2'd0);
    wire byte_at_1=tx_byte & (addr_phase_haddr[1:0]==2'd1);
    wire byte_at_2=tx_byte & (addr_phase_haddr[1:0]==2'd2);
    wire byte_at_3=tx_byte & (addr_phase_haddr[1:0]==2'd3);

    wire half_at_0=tx_half & (addr_phase_haddr[1:0]==2'd0);
    wire half_at_1=tx_half & (addr_phase_haddr[1:0]==2'd2);

    wire word_at_0=tx_word & (addr_phase_haddr[1:0]==2'd0);

    wire byte0=word_at_0 | half_at_0 | byte_at_0;
    wire byte1=word_at_0 | half_at_0 | byte_at_1;
    wire byte2=word_at_0 | half_at_1 | byte_at_2;
    wire byte3=word_at_0 | half_at_1 | byte_at_3;

    /* write */
    always @(posedge hclk or negedge hresetn)
    begin
        if(!hresetn)
        begin
            channel_reset <= 1'b0;
            configure <= 1'b0;
        end
        else
        begin
            /* ensure configure is only ever pulsed */
            configure <= 1'b0;

            if(addr_phase_hsel & addr_phase_hwrite & addr_phase_htrans[1])
            begin
                case(addr_phase_haddr[7:2])
                    status_addr: if(byte0) {configure, channel_reset} <= {hwdata[2], hwdata[0]};
                    correlator_addr: if(byte0) prn <= hwdata[4:0];
                    carrier_nco1_addr:
                    begin
                        if(byte0)
                            conf_carrier_theta[7:0] <= hwdata[7:0];
                        if(byte1)
                            conf_carrier_theta[15:8] <= hwdata[15:8];
                        if(byte2)
                            conf_carrier_theta[23:16] <= hwdata[23:16];
                        if(byte3)
                            conf_carrier_theta[29:24] <= hwdata[29:24];
                    end
                    carrier_nco2_addr:
                    begin
                        if(byte0)
                            conf_carrier_step[7:0] <= hwdata[7:0];
                        if(byte1)
                            conf_carrier_step[15:8] <= hwdata[15:8];
                        if(byte2)
                            conf_carrier_step[23:16] <= hwdata[23:16];
                        if(byte3)
                            conf_carrier_step[29:24] <= hwdata[29:24];
                    end
                    code_nco1_addr:
                    begin
                        if(byte0)
                            conf_code_number[7:0] <= hwdata[7:0];
                        if(byte1)
                            {conf_code_step[35:32], conf_code_number[9:8]} <= hwdata[13:8];
                    end
                    code_nco2_addr:
                    begin
                        if(byte0)
                            conf_code_step[7:0] <= hwdata[7:0];
                        if(byte1)
                            conf_code_step[15:8] <= hwdata[15:8];
                        if(byte2)
                            conf_code_step[23:16] <= hwdata[23:16];
                        if(byte3)
                            conf_code_step[31:24] <= hwdata[31:24];
                    end
                endcase
            end
        end
    end

    /* read */
    always @(posedge hclk)
    begin
        if(hready & hsel & (!hwrite) & htrans[1])
        begin
            case(haddr[7:2])
                status_addr:        hrdata <= {27'd0, pessimistic_lock, optimistic_lock, configure,
                                                correlator_running, channel_reset};
                correlator_addr:    hrdata <= {27'd0, prn};
                carrier_nco1_addr:  hrdata <= {2'd0, conf_carrier_theta};
                carrier_nco2_addr:  hrdata <= {2'd0, conf_carrier_step};
                code_nco1_addr:     hrdata <= {18'd0, conf_code_step[35:32], conf_code_number};
                code_nco2_addr:     hrdata <= conf_code_step[31:0];
                decoder_addr:       hrdata <= {TOW, 9'd0, enhanced_integrity, alert_flag, frame_valid,
                                                frame_ready};
                subframe_1a_addr:   hrdata <= {6'd0, week_number, SV_health, SV_health_bad, IODC};
                subframe_1b_addr:   hrdata <= {4'd0, URA_index, T_GD, t_oc};
                subframe_1c_addr:   hrdata <= {10'd0, a_f0};
                subframe_1d_addr:   hrdata <= {8'd0, a_f2, a_f1};
                subframe_2a_addr:   hrdata <= {Delta_n, 7'd0, curve_fit_greater, IODE_sf2};
                subframe_2b_addr:   hrdata <= M_0;
                subframe_2c_addr:   hrdata <= ecc;
                subframe_2d_addr:   hrdata <= sqrt_A;
                subframe_2e_addr:   hrdata <= {t_oe, C_rs};
                subframe_2f_addr:   hrdata <= {C_uc, C_us};
                subframe_3a_addr:   hrdata <= {2'd0, IDOT, 8'd0, IODE_sf3};
                subframe_3b_addr:   hrdata <= Omega_0;
                subframe_3c_addr:   hrdata <= incl_0;
                subframe_3d_addr:   hrdata <= omega; 
                subframe_3e_addr:   hrdata <= {8'd0, Omega_dot};
                subframe_3f_addr:   hrdata <= {16'd0, C_rc};
                subframe_3g_addr:   hrdata <= {C_ic, C_is};
                default: hrdata <= 32'd0;
            endcase
        end
    end

endmodule

