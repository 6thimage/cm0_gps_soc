module ahb_gpio32(
    input hclk,
    input hresetn,
    /* decode line */
    input hsel,
    /* inputs */
    input hready,
    input [31:0] haddr,
    input [1:0] htrans,
    input hwrite,
    input [2:0] hsize,
    input [31:0] hwdata,
    /* outputs */
    output hreadyout,
    output reg [31:0] hrdata,
    /* gpio */
    input [31:0] gpio_in,
    output reg [31:0] gpio_out,
    output reg [31:0] gpio_dir
);

assign hreadyout=1'b1;

/* address phase registers */
reg addr_phase_hsel;
reg [31:0] addr_phase_haddr;
reg [1:0] addr_phase_htrans;
reg addr_phase_hwrite;
reg [2:0] addr_phase_hsize;

/* capture address phase */
always @(posedge hclk or negedge hresetn)
begin
    if(!hresetn)
    begin
        addr_phase_hsel <= 1'b0;
        addr_phase_haddr <= 32'd0;
        addr_phase_htrans <= 2'd0;
        addr_phase_hwrite <= 1'b0;
        addr_phase_hsize <= 3'd0;
    end
    else if(hready)
    begin
        addr_phase_hsel <= hsel;
        addr_phase_haddr <= haddr;
        addr_phase_htrans <= htrans;
        addr_phase_hwrite <= hwrite;
        addr_phase_hsize <= hsize;
    end
end

/* address map
 * 
 * base +0x0    gpio dir (input low, output high)
 * base +0x4    gpio data
 */
localparam dir_addr=6'd0;
localparam data_addr=6'd1;

/* transaction size decode */
wire tx_byte=addr_phase_hsize[1:0]==2'b00;
wire tx_half=addr_phase_hsize[1:0]==2'b01;
wire tx_word=addr_phase_hsize[1:0]==2'b10;

wire byte_at_0=tx_byte & (addr_phase_haddr[1:0]==2'd0);
wire byte_at_1=tx_byte & (addr_phase_haddr[1:0]==2'd1);
wire byte_at_2=tx_byte & (addr_phase_haddr[1:0]==2'd2);
wire byte_at_3=tx_byte & (addr_phase_haddr[1:0]==2'd3);

wire half_at_0=tx_half & (addr_phase_haddr[1:0]==2'd0);
wire half_at_1=tx_half & (addr_phase_haddr[1:0]==2'd2);

wire word_at_0=tx_word & (addr_phase_haddr[1:0]==2'd0);

wire byte0=word_at_0 | half_at_0 | byte_at_0;
wire byte1=word_at_0 | half_at_0 | byte_at_1;
wire byte2=word_at_0 | half_at_1 | byte_at_2;
wire byte3=word_at_0 | half_at_1 | byte_at_3;

always @(posedge hclk or negedge hresetn)
begin
    if(!hresetn)
    begin
        gpio_dir <= 'd0;
        gpio_out <= 'd0;
    end
    /* write */
    else if(addr_phase_hsel & addr_phase_hwrite & addr_phase_htrans[1])
    begin
        if(addr_phase_haddr[7:2]==dir_addr)
        begin
            if(byte0)
                gpio_dir[7:0] <= hwdata[7:0];
            if(byte1)
                gpio_dir[15:8] <= hwdata[15:8];
            if(byte2)
                gpio_dir[23:16] <= hwdata[23:16];
            if(byte3)
                gpio_dir[31:24] <= hwdata[31:24];
        end
        else if(addr_phase_haddr[7:2]==data_addr)
        begin
            if(byte0)
                gpio_out[7:0] <= hwdata[7:0];
            if(byte1)
                gpio_out[15:8] <= hwdata[15:8];
            if(byte2)
                gpio_out[23:16] <= hwdata[23:16];
            if(byte3)
                gpio_out[31:24] <= hwdata[31:24];
        end
    end
end

always @(posedge hclk)
begin
    /* read */
    if(hready & hsel & (!hwrite) & htrans[1])
    begin
        if(addr_phase_haddr[7:2]==dir_addr)
            hrdata <= gpio_dir;
        else if(addr_phase_haddr[7:2]==data_addr)
            hrdata <= (gpio_dir&gpio_out) | ((~gpio_dir)&gpio_in);
        else
            hrdata <= 'd0;
    end
end

endmodule

