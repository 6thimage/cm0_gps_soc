module fifo_baseband_reader(
    input clk,
    input reset,
    /* fifo */
    output reg rd_en,
    input [7:0] rd_data,
    input rd_empty,
    /* baseband */
    output reg rf_data_valid,
    output reg [1:0] I, Q
    );

    initial
    begin
        rd_en <= 1'b0;
        rf_data_valid <= 1'b0;
    end

    /* states */
    localparam STATE_FETCH=0, STATE_NOP=1, STATE_WRITE=2;
    reg [1:0] state=STATE_FETCH;

    /* keeping track of which nibble is being sent */
    reg nibble=1'b0;

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            rd_en <= 1'b0;
            nibble <= 1'b0;
            rf_data_valid <= 1'b0;
        end
        else
        begin
            /* ensure read enable and data valud return low */
            rd_en <= 1'b0;
            rf_data_valid <= 1'b0;

            case(state)
            STATE_FETCH:
            begin
                /* only fetch data from the fifo if we are on a high nibble */
                if(!nibble && !rd_empty)
                begin
                    rd_en <= 1'b1;
                    state <= STATE_NOP;
                end
                else if(nibble)
                    state <= STATE_NOP;
            end

            STATE_NOP: state <= STATE_WRITE;

            STATE_WRITE:
            begin
                /* write nibble out */
                if(!nibble)
                    {I, Q} <= rd_data[7:4];
                else
                    {I, Q} <= rd_data[3:0];

                /* mark that there is valid data */
                rf_data_valid <= 1'b1;

                /* swap which nibble we are outputting */
                nibble <= ~nibble;

                /* move to the first state */
                state <= STATE_FETCH;
            end

            default: state <= STATE_FETCH;
            endcase
        end
    end

endmodule
