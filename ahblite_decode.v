module ahblite_decode #(
    parameter num_channels=1
    )
    (
    input [31:0] haddr,
    output hsel_ram,
    //output hsel_gpio,
    output hsel_ft245,
    output hsel_fft,
    output hsel_channel_group,
    output hsel_channel,
    output hsel_baseband_reader,
    output hsel_soc,
    output hsel_default,
    output reg [3:0] mux_sel
);

reg [8:0] dec;

/* processor memory map
 * 
 * addresses                    type                                hprot[3:2]  recommended use
 * 0x0000_0000 -> 0x1fff_ffff   normal (write through)              10          program code
 * 0x2000_0000 -> 0x3fff_ffff   normal (write back/write allocate)  11          on-chip ram
 * 0x4000_0000 -> 0x5fff_ffff   device                              01          peripherals
 * 0x6000_0000 -> 0x7fff_ffff   normal (write back/write allocate)  11          off-chip ram
 * 0x8000_0000 -> 0x9fff_ffff   normal (write through)              10          off-chip ram
 * 0xa000_0000 -> 0xdfff_ffff   device                              01          peripherals
 * 0xe000_0000 -> 0xefff_ffff   reserved                            --          NVIC etc.
 * 0xf000_0000 -> 0xffff_ffff   device                              01          none
 */

/* memory map */
assign hsel_ram=dec[0];             /* 0x0000_0000 -> 0x00ff_ffff (16MB) */
//assign hsel_gpio=dec[1];          /* 0x4000_0000 -> 0x40ff_ffff (16MB) */
assign hsel_ft245=dec[2];           /* 0x4100_0000 -> 0x41ff_ffff (16MB) */
assign hsel_fft=dec[3];             /* 0x4200_0000 -> 0x42ff_ffff (16MB) */
assign hsel_channel_group=dec[4];   /* 0x4400_0000 -> 0x44ff_ffff (16MB) */
assign hsel_channel=dec[5];         /* 0x4500_0000 -> 0x45ff_ffff (16MB) */
assign hsel_baseband_reader=dec[6]; /* 0x4600_0000 -> 0x46ff_ffff (16MB) */
assign hsel_soc=dec[7];             /* 0x4800_0000 -> 0x48ff_ffff (16MB) */
assign hsel_default=dec[8];

always @*
begin
    dec=9'b1_0000_0000;
    mux_sel=4'd8;
    case(haddr[31:24])
        8'h00:
        begin
            dec=9'b0_0000_0001;
            mux_sel=4'd0;
        end
        /*8'h40:
        begin
            dec=9'b0_0000_0010;
            mux_sel=4'd1;
        end*/
        8'h41:
        begin
            dec=9'b0_0000_0100;
            mux_sel=4'd2;
        end
        8'h42:
        begin
            dec=9'b0_0000_1000;
            mux_sel=4'd3;
        end
        8'h44:
        begin
            dec=9'b0_0001_0000;
            mux_sel=4'd4;
        end
        8'h45:
        begin
            if(haddr[13:8]<num_channels)
            begin
                dec=9'b0_0010_0000;
                mux_sel=4'd5;
            end
        end
        8'h46:
        begin
            dec=9'b0_0100_0000;
            mux_sel=4'd6;
        end
        8'h48:
        begin
            dec=9'b0_1000_0000;
            mux_sel=4'd7;
        end
    endcase
end

endmodule

