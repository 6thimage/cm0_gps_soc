module ahb_ft245 (
    input hclk,
    input hresetn,
    /* decode line */
    input hsel,
    /* inputs */
    input hready,
    input [31:0] haddr,
    input [1:0] htrans,
    input hwrite,
    input [2:0] hsize,
    input [31:0] hwdata,
    /* outputs */
    output hreadyout,
    output [31:0] hrdata,
    /* ft245 fifo connections */
    output reg rd_en,
    input [7:0] rd_data,
    input rd_empty,
    output reg wr_en,
    output reg [7:0] wr_data,
    input wr_full
);

/* address map
 *
 * base +0x0    status reg
 * base +0x4    write fifo
 * base +0x8    read fifo
 *
 * register map
 * status
 *      0     read empty
 *      1     write full
 *      2..31 unused
 */
localparam status_addr=6'd0;
localparam write_addr=6'd1;
localparam read_addr=6'd2;

initial
begin
    rd_en <= 1'b0;
    wr_en <= 1'b0;
end

/* address phase registers */
reg addr_phase_hsel=1'b0;
reg [31:0] addr_phase_haddr;
reg [1:0] addr_phase_htrans;
reg addr_phase_hwrite;
reg [2:0] addr_phase_hsize;

/* capture address phase */
always @(posedge hclk or negedge hresetn)
begin
    if(!hresetn)
    begin
        addr_phase_hsel <= 1'b0;
        addr_phase_haddr <= 32'd0;
        addr_phase_htrans <= 2'd0;
        addr_phase_hwrite <= 1'b0;
        addr_phase_hsize <= 3'd0;
    end
    else if(hready)
    begin
        addr_phase_hsel <= hsel;
        addr_phase_haddr <= haddr;
        addr_phase_htrans <= htrans;
        addr_phase_hwrite <= hwrite;
        addr_phase_hsize <= hsize;
    end
end

/* status register */
wire [31:0] status_reg={30'd0, wr_full, rd_empty};

/* transaction size decode */
wire tx_byte=addr_phase_hsize[1:0]==2'b00;
wire tx_half=addr_phase_hsize[1:0]==2'b01;
wire tx_word=addr_phase_hsize[1:0]==2'b10;

wire byte_at_0=tx_byte & (addr_phase_haddr[1:0]==2'd0);
wire byte_at_1=tx_byte & (addr_phase_haddr[1:0]==2'd1);
wire byte_at_2=tx_byte & (addr_phase_haddr[1:0]==2'd2);
wire byte_at_3=tx_byte & (addr_phase_haddr[1:0]==2'd3);

wire half_at_0=tx_half & (addr_phase_haddr[1:0]==2'd0);
wire half_at_1=tx_half & (addr_phase_haddr[1:0]==2'd2);

wire word_at_0=tx_word & (addr_phase_haddr[1:0]==2'd0);

wire byte0=word_at_0 | half_at_0 | byte_at_0;
wire byte1=word_at_0 | half_at_0 | byte_at_1;
wire byte2=word_at_0 | half_at_1 | byte_at_2;
wire byte3=word_at_0 | half_at_1 | byte_at_3;

/* write */
always @(posedge hclk or negedge hresetn)
begin
    if(!hresetn)
    begin
        wr_en <= 1'b0;
    end
    else
    begin
        wr_en <= 1'b0;

        if(addr_phase_hsel & addr_phase_hwrite & addr_phase_htrans[1])
        begin
            if(addr_phase_haddr[7:2]==write_addr)
            begin
                wr_en <= 1'b1;
                wr_data <= hwdata[7:0];
            end
        end
    end
end

/* read */
reg rd_en_delay=1'b0;
always @(posedge hclk or negedge hresetn)
begin
    if(!hresetn)
        rd_en_delay <= 1'b0;
    else
        rd_en_delay <= rd_en;
end
assign hrdata=(rd_en_delay)?{24'd0, rd_data}:status_reg;
assign hreadyout=!rd_en;

always @(posedge hclk)
begin
    if(!hresetn)
    begin
        rd_en <= 1'b0;
    end
    else
    begin
        rd_en <= 1'b0;

        if(hready & hsel & (!hwrite) & htrans[1])
        begin
            if(haddr[7:2]==read_addr)
                rd_en <= 1'b1;
        end
    end
end

endmodule

