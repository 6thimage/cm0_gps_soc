module ahblite_mux(
    input hclk,
    input hresetn,
    input [3:0] mux_sel,
    /* hrdata in */
    input [31:0] hrdata_ram,
    //input [31:0] hrdata_gpio,
    input [31:0] hrdata_ft245,
    input [31:0] hrdata_fft,
    input [31:0] hrdata_channel_group,
    input [31:0] hrdata_channel,
    input [31:0] hrdata_baseband_reader,
    input [31:0] hrdata_soc,
    input [31:0] hrdata_default,
    /* hready in */
    input hready_ram,
    //input hready_gpio,
    input hready_ft245,
    input hready_fft,
    input hready_channel_group,
    input hready_channel,
    input hready_baseband_reader,
    input hready_soc,
    input hready_default,
    /* hresp in */
    input hresp_ram,
    //input hresp_gpio,
    input hresp_ft245,
    input hresp_fft,
    input hresp_channel_group,
    input hresp_channel,
    input hresp_baseband_reader,
    input hresp_soc,
    input hresp_default,
    /* outputs */
    output reg [31:0] hrdata,
    output reg hready,
    output reg hresp
);

reg [3:0] addr_phase_mux_sel;

always @(posedge hclk)
begin
    if(!hresetn)
        addr_phase_mux_sel <= 4'd0;
    else if(hready)
        addr_phase_mux_sel <= mux_sel;
end

always @*
begin
    case(addr_phase_mux_sel)
        4'd0:
        begin
            hrdata=hrdata_ram;
            hready=hready_ram;
            hresp=hresp_ram;
        end
        /*4'd1:
        begin
            hrdata=hrdata_gpio;
            hready=hready_gpio;
            hresp=hresp_gpio;
        end*/
        4'd2:
        begin
            hrdata=hrdata_ft245;
            hready=hready_ft245;
            hresp=hresp_ft245;
        end
        4'd3:
        begin
            hrdata=hrdata_fft;
            hready=hready_fft;
            hresp=hresp_fft;
        end
        4'd4:
        begin
            hrdata=hrdata_channel_group;
            hready=hready_channel_group;
            hresp=hresp_channel_group;
        end
        4'd5:
        begin
            hrdata=hrdata_channel;
            hready=hready_channel;
            hresp=hresp_channel;
        end
        4'd6:
        begin
            hrdata=hrdata_baseband_reader;
            hready=hready_baseband_reader;
            hresp=hresp_baseband_reader;
        end
        4'd7:
        begin
            hrdata=hrdata_soc;
            hready=hready_soc;
            hresp=hresp_soc;
        end
        default:
        begin
            hrdata=hrdata_default;
            hready=hready_default;
            hresp=hresp_default;
        end
    endcase
end

endmodule

