module ahblite_sys_tb();
wire [7:0] Wing_A;
wire LED1;

always #1 clkin <= ~clkin;
reg clkin=1'b0;

/* AHB Lite bus */
/* master inputs */
wire hclk;
wire hresetn;
wire [31:0] hrdata;
wire hready;
wire hresp;
/* master outputs */
wire [31:0] haddr;
wire [2:0] hburst;
wire hmastlock;
wire [3:0] hprot;
wire [2:0] hsize;
wire [1:0] htrans;
wire [31:0] hwdata;
wire hwrite;

/* hclk assignment */
assign hclk=clkin;

/* reset logic */
/* hresetn needs to be low for 2 hclks, before it can go high */
wire sysresetreq;
reg [1:0] reset=2'd0;
reg pow_on_reset=1'b0;
initial
begin
    #0 pow_on_reset=1'b0;
    #1001 pow_on_reset=1'b1;
end
always @(posedge hclk or negedge pow_on_reset)
    if(!pow_on_reset)
        reset <= 2'd0;
    else
        reset <= {reset[0], ~sysresetreq};
assign hresetn=reset[1];

/* Cortex M0 processor */
wire lockup;
cortexm0ds cortex_m0(
    /* AHB Lite master */
    .hclk(hclk),
    .hresetn(hresetn),
    .haddr(haddr),
    .hburst(hburst),
    .hmastlock(hmastlock),
    .hprot(hprot),
    .hsize(hsize),
    .htrans(htrans),
    .hwdata(hwdata),
    .hwrite(hwrite),
    .hrdata(hrdata),
    .hready(hready),
    .hresp(hresp),
    /* Miscellaneous */
    .nmi(1'b0),
    .irq(16'd0),
    .txev(),
    .rxev(1'b0),
    .lockup(lockup),
    .sysresetreq(sysresetreq),
    .sleeping()
);

/* this line (currently) ensures that xst keeps the cortex_m0 module */
assign LED1=lockup;

assign hresp=1'b0; /* no errors generated by peripherals */

/* ram */
wire ram_hsel;
wire ram_hready;
wire [31:0] ram_hrdata;
ahb_ram #(.memory_width(12)) ram( /* 4kB ram */
    .hclk(hclk),
    .hresetn(hresetn),
    .hsel(ram_hsel),
    .hready(hready),
    .haddr(haddr),
    .htrans(htrans),
    .hwrite(hwrite),
    .hsize(hsize),
    .hwdata(hwdata),
    .hreadyout(ram_hready),
    .hrdata(ram_hrdata)
);

/* gpio */
wire gpio_hsel;
wire gpio_ready;
wire [31:0] gpio_hrdata;
wire [31:0] gpio_in, gpio_out, gpio_dir;
ahb_gpio32 gpio(
    .hclk(hclk),
    .hresetn(hresetn),
    .hsel(gpio_hsel),
    .hready(hready),
    .haddr(haddr),
    .htrans(htrans),
    .hwrite(hwrite),
    .hsize(hsize),
    .hwdata(hwdata),
    .hreadyout(gpio_hready),
    .hrdata(gpio_hrdata),
    .gpio_in(gpio_in),
    .gpio_out(gpio_out),
    .gpio_dir(gpio_dir)
);

genvar i;
generate
    for(i=0; i<8; i=i+1)
    begin :gpio_gen
        assign Wing_A[i]=(gpio_dir[i])?gpio_out[i]:1'bz;
    end
endgenerate
assign gpio_in={24'd0, Wing_A};

/* address decoder */
wire [3:0] mux_sel;
ahblite_decode decode(
    .haddr(haddr),
    .hsel_0(ram_hsel),
    .hsel_1(gpio_hsel),
    /*.hsel_2(),
    .hsel_3(),
    .hsel_4(),
    .hsel_5(),
    .hsel_6(),
    .hsel_7(),
    .hsel_8(),
    .hsel_9(),*/
    .hsel_nomap(),
    .mux_sel(mux_sel)
);

/* slave to master multiplexor */
ahblite_mux mux(
    .hclk(hclk),
    .hresetn(hresetn),
    .mux_sel(mux_sel),
    /* hrdata in */
    .hrdata_0(ram_hrdata),
    .hrdata_1(gpio_hrdata),
    /*.hrdata_2(),
    .hrdata_3(),
    .hrdata_4(),
    .hrdata_5(),
    .hrdata_6(),
    .hrdata_7(),
    .hrdata_8(),
    .hrdata_9(),*/
    .hrdata_nomap(32'hdeaddead),
    /* hready in */
    .hready_0(ram_hready),
    .hready_1(gpio_hready),
    /*.hready_2(),
    .hready_3(),
    .hready_4(),
    .hready_5(),
    .hready_6(),
    .hready_7(),
    .hready_8(),
    .hready_9(),*/
    .hready_nomap(1'b1),
    /* outputs */
    .hrdata(hrdata),
    .hready(hready)
);

/* sim commentary */
always @(posedge hresetn)
    $display("%t: sim leaving reset", $time);

always @(posedge hclk)
    if(hresetn & lockup)
    begin
        $display("%t: cpu lockup", $time);
        $finish(2);
    end

reg hwrite_last;
reg [1:0] htrans_last;
reg [31:0] haddr_last;
always @(posedge hclk)
    if(hready)
    begin
        htrans_last <= htrans;
        haddr_last <= haddr;
        hwrite_last <= hwrite;
    end

always @(posedge hclk)
begin
    if (hresetn & hready & htrans_last[1] & ~(ram_hsel | gpio_hsel))
        $display("%t: Warning, address %x selects neither RAM or gpio",
            $time, haddr_last);

    /*if (hresetn & hready & htrans_last[1] & ram_hsel)
    begin
        if(hwrite)
            $display("%t: ram write address %x data %x", $time, haddr_last, hwdata);
        else
            $display("%t: ram read  address %x data %x", $time, haddr_last, hrdata);
    end*/

    if (hresetn & hready & htrans_last[1] & gpio_hsel)
    begin
        if(hwrite)
            $display("%t: gpio write address %x data %x", $time, haddr_last, hwdata);
        else
            $display("%t: gpio read  address %x data %x", $time, haddr_last, hrdata);
    end
end
endmodule

