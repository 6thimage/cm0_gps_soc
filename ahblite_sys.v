module ahblite_sys(
    input clkin,
    output LED1,
    inout [7:0] usb_data,
    input usb_rxf, usb_txe,
    output usb_rd, usb_wr, usb_siwua,
    output sd_clk,
    inout sd_cmd,
    inout [3:0] sd_dat
    );

/* AHB Lite bus */
/* master inputs */
wire hclk;
wire hresetn;
wire [31:0] hrdata;
wire hready;
wire hresp;
/* master outputs */
wire [31:0] haddr;
//wire [2:0] hburst;
//wire hmastlock;
//wire [3:0] hprot;
wire [2:0] hsize;
wire [1:0] htrans;
wire [31:0] hwdata;
wire hwrite;

/* hclk assignment */
assign hclk=clkin;

/* reset logic */
/* prevent us from leaving reset, until we can output on the IO pins */
reg [9:0] reset_lockout='d0;
wire reset_locked=(reset_lockout!={10{1'b1}});
always @(posedge hclk) if(reset_locked) reset_lockout <= reset_lockout + 1'b1;
/* hresetn needs to be low for 2 hclks, before it can go high */
reg [2:0] reset=3'd0;
wire cm0_reset_request;
always @(posedge hclk)
begin
    if(cm0_reset_request)
        reset <= 'd0;
    else
        reset <= {reset[1:0], !reset_locked};
end
assign hresetn=reset[2];

/* interrupts */
wire fft_interrupt;
wire channel_group_interrupt;
wire [15:0] irq={14'd0, channel_group_interrupt, fft_interrupt};

/* Cortex M0 processor */
wire lockup;
cortexm0ds cortex_m0(
    /* AHB Lite master */
    .hclk(hclk),
    .hresetn(hresetn),
    .haddr(haddr),
    .hburst(/*hburst*/),
    .hmastlock(/*hmastlock*/),
    .hprot(/*hprot*/),
    .hsize(hsize),
    .htrans(htrans),
    .hwdata(hwdata),
    .hwrite(hwrite),
    .hrdata(hrdata),
    .hready(hready),
    .hresp(hresp),
    /* Miscellaneous */
    .nmi(1'b0),
    .irq(irq),
    .txev(),
    .rxev(1'b0),
    .lockup(lockup),
    .sysresetreq(cm0_reset_request),
    .sleeping()
);

assign LED1=lockup;

/* ram */
wire ram_hsel;
wire ram_hready;
wire [31:0] ram_hrdata;
ahb_ram #(.memory_width(13)) ram( /* 8 kB ram */
    .hclk(hclk),
    .hresetn(hresetn),
    .hsel(ram_hsel),
    .hready(hready),
    .haddr(haddr),
    .htrans(htrans),
    .hwrite(hwrite),
    .hsize(hsize),
    .hwdata(hwdata),
    .hreadyout(ram_hready),
    .hrdata(ram_hrdata)
);

/* ft245 fifo - depth of 8 = 256 bytes */
assign usb_siwua=1'b1;
wire ft245_fifo_rd_en, ft245_fifo_wr_en;
wire [7:0] ft245_fifo_rd_data, ft245_fifo_wr_data;
wire ft245_fifo_rd_empty, ft245_fifo_wr_full;
ft245_async_fifo #(.read_depth(8), .write_depth(8), .same_clocks(1))
                ft245_fifo(.D(usb_data), .RXFn(usb_rxf), .TXEn(usb_txe), .RDn(usb_rd), .WRn(usb_wr),
                           .clk_50mhz(hclk), .rw_clk(hclk),
                           .rd_en(ft245_fifo_rd_en), .rd_data(ft245_fifo_rd_data),
                               .rd_empty(ft245_fifo_rd_empty),
                           .wr_en(ft245_fifo_wr_en), .wr_data(ft245_fifo_wr_data),
                               .wr_full(ft245_fifo_wr_full));

/* ft245 */
wire ft245_hsel;
wire ft245_hready;
wire [31:0] ft245_hrdata;
wire ft245_rd_en, ft245_rd_empty;
wire [7:0] ft245_rd_data;
ahb_ft245 ft245(
    .hclk(hclk),
    .hresetn(hresetn),
    .hsel(ft245_hsel),
    .hready(hready),
    .haddr(haddr),
    .htrans(htrans),
    .hwrite(hwrite),
    .hsize(hsize),
    .hwdata(hwdata),
    .hreadyout(ft245_hready),
    .hrdata(ft245_hrdata),
    .rd_en(ft245_rd_en),
    .rd_data(ft245_rd_data),
    .rd_empty(ft245_rd_empty),
    .wr_en(ft245_fifo_wr_en),
    .wr_data(ft245_fifo_wr_data),
    .wr_full(ft245_fifo_wr_full)
);

/* fifo baseband reader */
wire fifo_baseband_rd_en, fifo_baseband_rd_empty;
wire [7:0] fifo_baseband_rd_data;
wire fifo_rf_valid;
wire [1:0] fifo_I, fifo_Q;
fifo_baseband_reader fifo_baseband(.clk(hclk), .reset(hresetn),
                                   .rd_en(fifo_baseband_rd_en), .rd_data(fifo_baseband_rd_data),
                                       .rd_empty(fifo_baseband_rd_empty),
                                   .rf_data_valid(fifo_rf_valid), .I(fifo_I), .Q(fifo_Q));

/* read fifo mux */
wire ft245_mux_enable, ft245_mux_mode;
wire [15:0] ft245_mux_count;
read_fifo_mux ft245_mux(.clk(hclk),
                        .mux_en(ft245_mux_enable), .mode(ft245_mux_mode),
                            .count(ft245_mux_count), .is_counting(),
                        .read_en(ft245_fifo_rd_en), .read_data(ft245_fifo_rd_data),
                            .read_empty(ft245_fifo_rd_empty),
                        .A_read_en(ft245_rd_en), .A_read_data(ft245_rd_data),
                            .A_read_empty(ft245_rd_empty),
                        .B_read_en(fifo_baseband_rd_en), .B_read_data(fifo_baseband_rd_data),
                            .B_read_empty(fifo_baseband_rd_empty));

/* baseband mux outputs */
wire baseband_mode;
reg rf_data_valid;
reg [1:0] rf_I, rf_Q;

/* fft module */
wire fft_hsel;
wire fft_hready;
wire [31:0] fft_hrdata;
ahb_fft fft(
    .hclk(hclk),
    .hresetn(hresetn),
    .hsel(fft_hsel),
    .hready(hready),
    .haddr(haddr),
    .htrans(htrans),
    .hwrite(hwrite),
    .hsize(hsize),
    .hwdata(hwdata),
    .hreadyout(fft_hready),
    .hrdata(fft_hrdata),
    .interrupt(fft_interrupt),
    .baseband_mode(baseband_mode),
    .rf_data_valid(rf_data_valid),
    .I(rf_I),
    .Q(rf_Q)
);

localparam num_channels=4;

/* channel group */
wire channel_group_hsel;
wire channel_group_hready;
wire [31:0] channel_group_hrdata;

wire signed [15:0] carrier_c1, carrier_c2;
wire signed [25:0] code_c1, code_c2;
wire [num_channels-1:0] channel_group_channel_active;
wire [num_channels-1:0] channel_group_frame_ready;
wire [(17*num_channels)-1:0] channel_group_TOW;
wire [(10*num_channels)-1:0] channel_group_IODC;
wire [num_channels-1:0] channel_group_IOD_match;
wire [num_channels-1:0] channel_group_code_num_req;
wire [(46*num_channels)-1:0] channel_group_code_num;
ahb_channel_group #(.num_channels(num_channels)) channel_group(
    .hclk(hclk),
    .hresetn(hresetn),
    .hsel(channel_group_hsel),
    .hready(hready),
    .haddr(haddr),
    .htrans(htrans),
    .hwrite(hwrite),
    .hsize(hsize),
    .hwdata(hwdata),
    .hreadyout(channel_group_hready),
    .hrdata(channel_group_hrdata),
    .interrupt(channel_group_interrupt),

    .rf_data_valid(rf_data_valid),
    .baseband_mode(baseband_mode),
    .carrier_c1(carrier_c1),
    .carrier_c2(carrier_c2),
    .code_c1(code_c1),
    .code_c2(code_c2),
    .channel_active(channel_group_channel_active),
    .frame_ready(channel_group_frame_ready),
    .TOW(channel_group_TOW),
    .IODC(channel_group_IODC),
    .IOD_match(channel_group_IOD_match),
    .code_num_req(channel_group_code_num_req),
    .code_num(channel_group_code_num)
);

/* channels */
wire channel_hsel;
reg channel_hready;
reg [31:0] channel_hrdata;
reg [num_channels:0] channel_sub_hsel;
wire [num_channels-1:0] channel_sub_hready;
wire [31:0] channel_sub_hrdata[0:num_channels-1];
reg [4:0] channel_sub_mux, addr_phase_sub_mux;

always @(posedge hclk)
begin
    if(!hresetn)
        addr_phase_sub_mux <= 'd0;
    else if(hready)
        addr_phase_sub_mux <= channel_sub_mux;
end

integer j;
always @*
begin
    channel_sub_hsel='d0;
    channel_sub_mux='d0;

    if(channel_hsel)
    begin
        for(j=0; j<num_channels; j=j+1)
        begin
            if(haddr[13:8]==j)
            begin
                channel_sub_hsel=1<<j;
                channel_sub_mux=j;
            end
        end
    end
end

genvar i;
generate
    for(i=0; i<num_channels; i=i+1)
    begin :channel_gen
        ahb_channel channel(
            .hclk(hclk),
            .hresetn(hresetn),
            .hsel(channel_sub_hsel[i]),
            .hready(hready),
            .haddr(haddr),
            .htrans(htrans),
            .hwrite(hwrite),
            .hsize(hsize),
            .hwdata(hwdata),
            .hreadyout(channel_sub_hready[i]),
            .hrdata(channel_sub_hrdata[i]),
            .rf_data_valid(rf_data_valid),
            .I(rf_I),
            .Q(rf_Q),
            .baseband_mode(baseband_mode),
            .carrier_c1(carrier_c1),
            .carrier_c2(carrier_c2),
            .code_c1(code_c1),
            .code_c2(code_c2),
            .channel_active(channel_group_channel_active[i]),
            .frame_ready(channel_group_frame_ready[i]),
            .TOW(channel_group_TOW[((i+1)*17)-1:i*17]),
            .IODC(channel_group_IODC[((i+1)*10)-1:i*10]),
            .IOD_match(channel_group_IOD_match[i]),
            .code_num_req(channel_group_code_num_req[i]),
            .code_num(channel_group_code_num[((i+1)*46)-1:i*46])
        );
    end
endgenerate

always @*
begin
    channel_hrdata=channel_sub_hrdata[0];
    channel_hready=channel_sub_hready[0];

    for(j=0; j<num_channels; j=j+1)
    begin
        if(addr_phase_sub_mux==j)
        begin
            channel_hrdata=channel_sub_hrdata[j];
            channel_hready=channel_sub_hready[j];
        end
    end
end

/* baseband reader */
wire baseband_reader_hsel;
wire baseband_reader_hready;
wire [31:0] baseband_reader_hrdata;
wire baseband_reader_rf_valid;
wire [1:0] baseband_reader_I, baseband_reader_Q;
ahb_baseband_reader baseband_reader(
    .hclk(hclk),
    .hresetn(hresetn),
    .hsel(baseband_reader_hsel),
    .hready(hready),
    .haddr(haddr),
    .htrans(htrans),
    .hwrite(hwrite),
    .hsize(hsize),
    .hwdata(hwdata),
    .hreadyout(baseband_reader_hready),
    .hrdata(baseband_reader_hrdata),
    .sd_clk(sd_clk),
    .sd_cmd(sd_cmd),
    .sd_data(sd_dat),
    .rf_data_valid(baseband_reader_rf_valid),
    .I(baseband_reader_I),
    .Q(baseband_reader_Q)
);

/* baseband mux */
wire baseband_mux;
always @*
begin
    if(baseband_mux)
    begin
        rf_data_valid = fifo_rf_valid;
        rf_I = fifo_I;
        rf_Q = fifo_Q;
    end
    else
    begin
        rf_data_valid = baseband_reader_rf_valid;
        rf_I = baseband_reader_I;
        rf_Q = baseband_reader_Q;
    end
end

/* soc control */
wire soc_con_hsel;
wire soc_con_hready;
wire [31:0] soc_con_hrdata;
ahb_soc_control soc_control(
    .hclk(hclk),
    .hresetn(hresetn),
    .hsel(soc_con_hsel),
    .hready(hready),
    .haddr(haddr),
    .htrans(htrans),
    .hwrite(hwrite),
    .hsize(hsize),
    .hwdata(hwdata),
    .hreadyout(soc_con_hready),
    .hrdata(soc_con_hrdata),
    .ft245_read_fifo_enable(ft245_mux_enable),
    .ft245_read_fifo_mode(ft245_mux_mode),
    .ft245_read_fifo_count(ft245_mux_count),
    .baseband_mux(baseband_mux)
);

/* default slave */
wire default_hsel;
wire default_hready;
wire default_hresp;
ahb_default default_slave(
    .hclk(hclk),
    .hresetn(hresetn),
    .hsel(default_hsel),
    .hready(hready),
    .htrans(htrans),
    .hreadyout(default_hready),
    .hrespout(default_hresp)
);

/* address decoder */
wire [3:0] mux_sel;
ahblite_decode #(.num_channels(num_channels)) decode(
    .haddr(haddr),
    .hsel_ram(ram_hsel),
    //.hsel_gpio(/*gpio_hsel*/),
    .hsel_ft245(ft245_hsel),
    .hsel_fft(fft_hsel),
    .hsel_channel_group(channel_group_hsel),
    .hsel_channel(channel_hsel),
    .hsel_baseband_reader(baseband_reader_hsel),
    .hsel_soc(soc_con_hsel),
    .hsel_default(default_hsel),
    .mux_sel(mux_sel)
);

/* slave to master multiplexor */
ahblite_mux mux(
    .hclk(hclk),
    .hresetn(hresetn),
    .mux_sel(mux_sel),
    /* hrdata in */
    .hrdata_ram(ram_hrdata),
    //.hrdata_gpio(/*gpio_hrdata*/),
    .hrdata_ft245(ft245_hrdata),
    .hrdata_fft(fft_hrdata),
    .hrdata_channel_group(channel_group_hrdata),
    .hrdata_channel(channel_hrdata),
    .hrdata_baseband_reader(baseband_reader_hrdata),
    .hrdata_soc(soc_con_hrdata),
    .hrdata_default(32'hdeaddead),
    /* hready in */
    .hready_ram(ram_hready),
    //.hready_gpio(/*gpio_hready*/),
    .hready_ft245(ft245_hready),
    .hready_fft(fft_hready),
    .hready_channel_group(channel_group_hready),
    .hready_channel(channel_hready),
    .hready_baseband_reader(baseband_reader_hready),
    .hready_soc(soc_con_hready),
    .hready_default(default_hready),
    /* hresp in */
    .hresp_ram(1'b0),
    //.hresp_gpio(1'b0),
    .hresp_ft245(1'b0),
    .hresp_fft(1'b0),
    .hresp_channel_group(1'b0),
    .hresp_channel(1'b0),
    .hresp_baseband_reader(1'b0),
    .hresp_soc(1'b0),
    .hresp_default(default_hresp),
    /* outputs */
    .hrdata(hrdata),
    .hready(hready),
    .hresp(hresp)
);

endmodule

