module ahb_baseband_reader(
    input hclk,
    input hresetn,
    /* decode line */
    input hsel,
    /* inputs */
    input hready,
    input [31:0] haddr,
    input [1:0] htrans,
    input hwrite,
    input [2:0] hsize,
    input [31:0] hwdata,
    /* outputs */
    output hreadyout,
    output reg [31:0] hrdata,
    /* sd card */
    output sd_clk,
    inout sd_cmd,
    inout [3:0] sd_data,
    /* baseband */
    output rf_data_valid,
    output [1:0] I, Q
    );

    /* address map
     *              register
     * base +0x0    status
     * base +0x4    start block
     * base +0x8    number of blocks
     *
     * register map
     *                   |33222222|22221111|11111100|00000000|
     *                   |10987654|32109876|54321098|76543210|
     * status            |********|********|********|***EIRPA|
     * start block       |<           start block           >|
     * number of blocks  |<        number of blocks         >|
     *                   |********|********|********|********|
     *
     * status
     *      0       active
     *      1       enable playback
     *      2       allow restart
     *      3       idle
     *      4       error
     *      5..31   unused
     * start block
     *      0..31   start block
     * number of blocks
     *      0..31   number of blocks
     */
    localparam status_addr=6'd0,
               start_block_addr=6'd1,
               num_block_addr=6'd2;

    assign hreadyout=1'b1;

    reg reader_reset=1'b0;
    reg [31:0] start_block, number_of_blocks;
    reg enable_playback, allow_restart;
    wire idle, error;
    baseband_reader bb_reader(.clk(hclk), .reset(reader_reset),
                              .sd_clk(sd_clk), .sd_cmd(sd_cmd), .sd_data(sd_data),
                              .data_valid(rf_data_valid), .I(I), .Q(Q),
                              .start_block(start_block), .number_of_blocks(number_of_blocks),
                                  .enable_playback(enable_playback), .allow_restart(allow_restart),
                              .idle(idle), .error(error));

    /* address phase registers */
    reg addr_phase_hsel=1'b0;
    reg [31:0] addr_phase_haddr;
    reg [1:0] addr_phase_htrans;
    reg addr_phase_hwrite;
    reg [2:0] addr_phase_hsize;

    /* capture address phase */
    always @(posedge hclk or negedge hresetn)
    begin
        if(!hresetn)
        begin
            addr_phase_hsel <= 1'b0;
            addr_phase_haddr <= 32'd0;
            addr_phase_htrans <= 2'd0;
            addr_phase_hwrite <= 1'b0;
            addr_phase_hsize <= 3'd0;
        end
        else if(hready)
        begin
            addr_phase_hsel <= hsel;
            addr_phase_haddr <= haddr;
            addr_phase_htrans <= htrans;
            addr_phase_hwrite <= hwrite;
            addr_phase_hsize <= hsize;
        end
    end

    /* transaction size decode */
    wire tx_byte=addr_phase_hsize[1:0]==2'b00;
    wire tx_half=addr_phase_hsize[1:0]==2'b01;
    wire tx_word=addr_phase_hsize[1:0]==2'b10;

    wire byte_at_0=tx_byte & (addr_phase_haddr[1:0]==2'd0);
    wire byte_at_1=tx_byte & (addr_phase_haddr[1:0]==2'd1);
    wire byte_at_2=tx_byte & (addr_phase_haddr[1:0]==2'd2);
    wire byte_at_3=tx_byte & (addr_phase_haddr[1:0]==2'd3);

    wire half_at_0=tx_half & (addr_phase_haddr[1:0]==2'd0);
    wire half_at_1=tx_half & (addr_phase_haddr[1:0]==2'd2);

    wire word_at_0=tx_word & (addr_phase_haddr[1:0]==2'd0);

    wire byte0=word_at_0 | half_at_0 | byte_at_0;
    wire byte1=word_at_0 | half_at_0 | byte_at_1;
    wire byte2=word_at_0 | half_at_1 | byte_at_2;
    wire byte3=word_at_0 | half_at_1 | byte_at_3;

    /* write */
    always @(posedge hclk or negedge hresetn)
    begin
        if(!hresetn)
        begin
            reader_reset <= 1'b0;
        end
        else
        begin
            if(addr_phase_hsel & addr_phase_hwrite & addr_phase_htrans[1])
            begin
                case(addr_phase_haddr[7:2])
                    status_addr: if(byte0) {allow_restart, enable_playback, reader_reset} <= hwdata[2:0];
                    start_block_addr:
                    begin
                        if(byte0)
                            start_block[7:0] <= hwdata[7:0];
                        if(byte1)
                            start_block[15:8] <= hwdata[15:8];
                        if(byte2)
                            start_block[23:16] <= hwdata[23:16];
                        if(byte3)
                            start_block[31:24] <= hwdata[31:24];
                    end
                    num_block_addr:
                    begin
                        if(byte0)
                            number_of_blocks[7:0] <= hwdata[7:0];
                        if(byte1)
                            number_of_blocks[15:8] <= hwdata[15:8];
                        if(byte2)
                            number_of_blocks[23:16] <= hwdata[23:16];
                        if(byte3)
                            number_of_blocks[31:24] <= hwdata[31:24];
                    end
                endcase
            end
        end
    end

    /* read */
    always @(posedge hclk)
    begin
        if(hready & hsel & (!hwrite) & htrans[1])
        begin
            case(haddr[7:2])
                status_addr:      hrdata <= {27'd0, error, idle, allow_restart, enable_playback, reader_reset};
                start_block_addr: hrdata <= start_block;
                num_block_addr:   hrdata <= number_of_blocks;
                default:          hrdata <= 32'd0;
            endcase
        end
    end

endmodule

