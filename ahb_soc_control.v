module ahb_soc_control (
    input hclk,
    input hresetn,
    /* decode line */
    input hsel,
    /* inputs */
    input hready,
    input [31:0] haddr,
    input [1:0] htrans,
    input hwrite,
    input [2:0] hsize,
    input [31:0] hwdata,
    /* outputs */
    output hreadyout,
    output reg [31:0] hrdata,
    /* ft245 in fifo mux control */
    output reg ft245_read_fifo_enable,
    output reg ft245_read_fifo_mode,
    output reg [15:0] ft245_read_fifo_count,
    /* baseband mux control */
    output reg baseband_mux
    );

    /* address map
     *              register
     * base +0x0    ft245 fifo
     *
     * register map
     *                   |33222222|22221111|11111100|00000000|
     *                   |10987654|32109876|54321098|76543210|
     * ft245 fifo        |<  fifo count   >|********|******ME|
     * baseband          |********|********|********|*******M|
     *
     * ft245 fifo
     *   When mode is zero, the enable is a simple mux select line, with 0 being AHB and
     *   1 being the baseband reader. When mode is 1, the mux will select the baseband
     *   reader for count reads of the fifo and then return it to the AHB
     *      0       read fifo enable
     *      1       read fifo mode
     *      2..15   unused
     *      16..31  read fifo count
     *
     * baseband
     *      0       mode - 0 for SD card, 1 for FT245 (USB)
     *      1..31   unused
     */

    localparam ft245_fifo_addr=6'd0,
               baseband_addr=6'd1;

    initial
    begin
        ft245_read_fifo_enable <= 1'b0;
        ft245_read_fifo_mode <= 1'b0;
        ft245_read_fifo_count <= 'd0;

        baseband_mux <= 1'b0;
    end

    assign hreadyout=1'b1;

    /* address phase registers */
    reg addr_phase_hsel=1'b0;
    reg [31:0] addr_phase_haddr;
    reg [1:0] addr_phase_htrans;
    reg addr_phase_hwrite;
    reg [2:0] addr_phase_hsize;

    /* capture address phase */
    always @(posedge hclk or negedge hresetn)
    begin
        if(!hresetn)
        begin
            addr_phase_hsel <= 1'b0;
            addr_phase_haddr <= 32'd0;
            addr_phase_htrans <= 2'd0;
            addr_phase_hwrite <= 1'b0;
            addr_phase_hsize <= 3'd0;
        end
        else if(hready)
        begin
            addr_phase_hsel <= hsel;
            addr_phase_haddr <= haddr;
            addr_phase_htrans <= htrans;
            addr_phase_hwrite <= hwrite;
            addr_phase_hsize <= hsize;
        end
    end

    /* transaction size decode */
    wire tx_byte=addr_phase_hsize[1:0]==2'b00;
    wire tx_half=addr_phase_hsize[1:0]==2'b01;
    wire tx_word=addr_phase_hsize[1:0]==2'b10;

    wire byte_at_0=tx_byte & (addr_phase_haddr[1:0]==2'd0);
    wire byte_at_1=tx_byte & (addr_phase_haddr[1:0]==2'd1);
    wire byte_at_2=tx_byte & (addr_phase_haddr[1:0]==2'd2);
    wire byte_at_3=tx_byte & (addr_phase_haddr[1:0]==2'd3);

    wire half_at_0=tx_half & (addr_phase_haddr[1:0]==2'd0);
    wire half_at_1=tx_half & (addr_phase_haddr[1:0]==2'd2);

    wire word_at_0=tx_word & (addr_phase_haddr[1:0]==2'd0);

    wire byte0=word_at_0 | half_at_0 | byte_at_0;
    wire byte1=word_at_0 | half_at_0 | byte_at_1;
    wire byte2=word_at_0 | half_at_1 | byte_at_2;
    wire byte3=word_at_0 | half_at_1 | byte_at_3;

    /* write */
    always @(posedge hclk)
    begin
        if(addr_phase_hsel & addr_phase_hwrite & addr_phase_htrans[1])
        begin
            case(addr_phase_haddr[7:2])
                ft245_fifo_addr:
                begin
                    if(byte0)
                        {ft245_read_fifo_mode, ft245_read_fifo_enable} <= hwdata[1:0];
                    if(byte2)
                        ft245_read_fifo_count[7:0] <= hwdata[23:16];
                    if(byte3)
                        ft245_read_fifo_count[15:8] <= hwdata[31:24];
                end
                baseband_addr: if(byte0) baseband_mux <= hwdata[0];
            endcase
        end
    end

    /* read */
    always @(posedge hclk)
    begin
        if(hready & hsel & (!hwrite) & htrans[1])
        begin
            case(haddr[7:2])
                ft245_fifo_addr: hrdata <= {ft245_read_fifo_count, 14'd0, ft245_read_fifo_mode,
                                            ft245_read_fifo_enable};
                baseband_addr:   hrdata <= {31'd0, baseband_mux};
                default:         hrdata <= 32'd0;
            endcase
        end
    end

endmodule

