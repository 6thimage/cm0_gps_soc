module ahb_fft (
    input hclk,
    input hresetn,
    /* decode line */
    input hsel,
    /* inputs */
    input hready,
    input [31:0] haddr,
    input [1:0] htrans,
    input hwrite,
    input [2:0] hsize,
    input [31:0] hwdata,
    /* outputs */
    output hreadyout,
    output reg [31:0] hrdata,
    /* interrupt */
    output interrupt,
    /* baseband */
    input baseband_mode,
    input rf_data_valid,
    input [1:0] I, Q
);

/* address map
 *              register
 * base +0x0    status
 * base +0x4    frequency
 * base +0x8    satellite
 *
 * base +0x10   interrupt enable
 * base +0x14   interrupt flags
 *
 * base +0x20   satellite number & doppler
 * base +0x24   satellite peak
 * base +0x28   satellite mean
 *
 * register map
 *                   |33222222|22221111|11111100|00000000|
 *                   |10987654|32109876|54321098|76543210|
 * status            |********|********|********|******EI|
 * frequency         |**<dopp>|********|**< centre freq >|
 * satellite         |<       satellite bit field       >|
 * interrupt enable  |********|********|********|*******C|
 * interrupt flags   |********|********|********|*******C|
 * sat num & doppler |********|********|*<dopp >|***<num>|
 * satellite peak    |<   location  ><       peak       >|
 * satellite mean    |********|******<       mean       >|
 *
 * status
 *      0      idle
 *      1      enable (auto-clears on falling edge of idle)
 *      2..31  unused
 * frequency
 *      0..13  rf centre frequency
 *      14..23 unused
 *      24..29 doppler side shift
 *      30..31 unused
 * satellite
 *      0..31  satellite bit field (bit 0 for PRN 1, bit 12 for PRN 13 etc., high is enabled)
 * interrupt enable
 *      0      enable interrupt on each satellite doppler complete
 *      1..31  unused
 * interrupt flag
 *      0      set on each satellite doppler complete
 *      1..31  unused
 * satellite number & doppler
 *      0..4   satellite number
 *      5..7   unused
 *      8..14  doppler shift
 *      15..31 unused
 * satellite peak
 *      0..17  peak
 *      18..31 location
 * satellite mean
 *      0..17  mean
 *      18..31 unused
 */
localparam status_addr=6'd0;
localparam frequency_addr=6'd1;
localparam satellite_addr=6'd2;
/* 6'd3 */
localparam interrupt_enable_addr=6'd4;
localparam interrupt_flags_addr=6'd5;
/* 6'd6 */
/* 6'd7 */
localparam satellite_num_dopp_addr=6'd8;
localparam satellite_peak_addr=6'd9;
localparam satellite_mean_addr=6'd10;

assign hreadyout=1'b1;

/* address phase registers */
reg addr_phase_hsel=1'b0;
reg [31:0] addr_phase_haddr;
reg [1:0] addr_phase_htrans;
reg addr_phase_hwrite;
reg [2:0] addr_phase_hsize;

/* capture address phase */
always @(posedge hclk or negedge hresetn)
begin
    if(!hresetn)
    begin
        addr_phase_hsel <= 1'b0;
        addr_phase_haddr <= 32'd0;
        addr_phase_htrans <= 2'd0;
        addr_phase_hwrite <= 1'b0;
        addr_phase_hsize <= 3'd0;
    end
    else if(hready)
    begin
        addr_phase_hsel <= hsel;
        addr_phase_haddr <= haddr;
        addr_phase_htrans <= htrans;
        addr_phase_hwrite <= hwrite;
        addr_phase_hsize <= hsize;
    end
end

/* fft module */
reg [13:0] rf_centre_frequency;
reg [5:0] doppler_side_shift;
reg [31:0] satellites;
wire satellite_valid;
wire [4:0] fft_satellite_number;
wire fft_satellite_doppler_sub;
wire signed [6:0] fft_satellite_doppler;
wire [17:0] fft_satellite_peak;
wire [13:0] fft_satellite_peak_location;
wire [16:0] fft_satellite_mean;
wire fft_satellite_mean_overflow;
wire fft_idle;
reg fft_enable=1'b0;
fft_module fft(.clk(hclk), .reset(hresetn),
               .enable(fft_enable),
               .baseband_mode(baseband_mode), .rf_data_valid(rf_data_valid), .I(I), .Q(Q),
               .rf_centre_frequency(rf_centre_frequency), .doppler_side_shift(doppler_side_shift),
               .satellites(satellites),
               .satellite_valid(satellite_valid), .satellite_number(fft_satellite_number),
               .satellite_doppler(fft_satellite_doppler),
               .satellite_peak(fft_satellite_peak),
               .satellite_peak_location(fft_satellite_peak_location),
               .satellite_mean(fft_satellite_mean),
               .satellite_mean_overflow(fft_satellite_mean_overflow),
               .idle(fft_idle)
              );

/* */
reg [4:0] satellite_number;
reg signed [6:0] satellite_doppler;
reg [17:0] satellite_peak;
reg [13:0] satellite_peak_location;
reg [16:0] satellite_mean;
reg satellite_mean_overflow;
always @(posedge hclk)
begin
    if(satellite_valid)
    begin
        satellite_number <= fft_satellite_number;
        satellite_doppler <= fft_satellite_doppler;
        satellite_peak <= fft_satellite_peak;
        satellite_peak_location <= fft_satellite_peak_location;
        satellite_mean <= fft_satellite_mean;
        satellite_mean_overflow <= fft_satellite_mean_overflow;
    end
end

/* transaction size decode */
wire tx_byte=addr_phase_hsize[1:0]==2'b00;
wire tx_half=addr_phase_hsize[1:0]==2'b01;
wire tx_word=addr_phase_hsize[1:0]==2'b10;

wire byte_at_0=tx_byte & (addr_phase_haddr[1:0]==2'd0);
wire byte_at_1=tx_byte & (addr_phase_haddr[1:0]==2'd1);
wire byte_at_2=tx_byte & (addr_phase_haddr[1:0]==2'd2);
wire byte_at_3=tx_byte & (addr_phase_haddr[1:0]==2'd3);

wire half_at_0=tx_half & (addr_phase_haddr[1:0]==2'd0);
wire half_at_1=tx_half & (addr_phase_haddr[1:0]==2'd2);

wire word_at_0=tx_word & (addr_phase_haddr[1:0]==2'd0);

wire byte0=word_at_0 | half_at_0 | byte_at_0;
wire byte1=word_at_0 | half_at_0 | byte_at_1;
wire byte2=word_at_0 | half_at_1 | byte_at_2;
wire byte3=word_at_0 | half_at_1 | byte_at_3;

/* interrupts */
reg interrupt_flag_doppler_complete=1'b0;
wire [31:0] interrupt_flags={30'd0, interrupt_flag_doppler_complete};

reg interrupt_enable_doppler_complete=1'b0;
wire [31:0] interrupt_enable={30'd0, interrupt_enable_doppler_complete};

assign interrupt=((interrupt_enable&interrupt_flags)!='d0);

/* write */
always @(posedge hclk or negedge hresetn)
begin
    if(!hresetn)
    begin
        fft_enable <= 1'b0;
        interrupt_flag_doppler_complete <= 1'b0;
    end
    else
    begin
        /* auto return of fft enable */
        if(fft_enable && !fft_idle)
            fft_enable <= 1'b0;

        if(addr_phase_hsel & addr_phase_hwrite & addr_phase_htrans[1])
        begin
            case(addr_phase_haddr[7:2])
                status_addr: if(byte0) fft_enable <= hwdata[1];
                frequency_addr:
                begin
                    if(byte0)
                        rf_centre_frequency[7:0] <= hwdata[7:0];
                    if(byte1)
                        rf_centre_frequency[13:8] <= hwdata[13:8];
                    if(byte3)
                        doppler_side_shift <= hwdata[29:24];
                end
                satellite_addr:
                begin
                    if(byte0)
                        satellites[7:0] <= hwdata[7:0];
                    if(byte1)
                        satellites[15:8] <= hwdata[15:8];
                    if(byte2)
                        satellites[23:16] <= hwdata[23:16];
                    if(byte3)
                        satellites[31:24] <= hwdata[31:24];
                end
                interrupt_enable_addr: if(byte0) interrupt_enable_doppler_complete <= hwdata[0];
                interrupt_flags_addr: if(byte0) interrupt_flag_doppler_complete <= hwdata[0];
            endcase
        end

        if(satellite_valid)
            interrupt_flag_doppler_complete <= 1'b1;
    end
end

/* read */
always @(posedge hclk)
begin
    if(hready & hsel & (!hwrite) & htrans[1])
    begin
        case(haddr[7:2])
            status_addr: hrdata <= {30'd0, fft_enable, fft_idle};
            frequency_addr: hrdata <= {2'd0, doppler_side_shift, 10'd0, rf_centre_frequency};
            satellite_addr: hrdata <= satellites;
            interrupt_enable_addr: hrdata <= interrupt_enable;
            interrupt_flags_addr: hrdata <= interrupt_flags;
            satellite_num_dopp_addr: hrdata <= {17'd0, satellite_doppler, 3'd0, satellite_number};
            satellite_peak_addr: hrdata <= {satellite_peak_location, satellite_peak};
            satellite_mean_addr: hrdata <= {14'd0, satellite_mean_overflow, satellite_mean};
            default: hrdata <= 'd0;
        endcase
    end
end

endmodule

