#! /usr/bin/env python

import argparse
import ftdi1 as ftdi
from os import SEEK_SET
from sys import stdout

parser=argparse.ArgumentParser()
parser.add_argument('command', help='command to send', type=str)
parser.add_argument('file', help='name of file to send', type=str)

args=parser.parse_args()

with open(args.file, 'rb') as f:
    context=ftdi.new()
    print 'libftdi {}'.format(ftdi.get_library_version().version_str)
    ret, dev_list=ftdi.usb_find_all(context, 0, 0)
    if ret<0:
        print 'Failed to find device'
        exit()
    if not dev_list:
        print 'No devices found'
        exit()

    # print devices
    node=dev_list
    while node:
        ret, manu, desc, ser=ftdi.usb_get_strings(context, node.dev)
        if ret>=0:
            print'{} {} {}'.format(manu, desc, ser)
        node=node.next

    # open interface
    ftdi.set_interface(context, ftdi.INTERFACE_B)
    ret=ftdi.usb_open_dev(context, dev_list.dev)
    ftdi.list_free2(dev_list)
    if ret<0:
        print 'Failed to open device interface'
        exit()

    # set mode
    ret=ftdi.set_bitmode(context, 0xff, ftdi.BITMODE_SYNCFF)
    if ret<0:
        print 'Failed to set mode'
        exit()

    # read any data waiting
    ret, read_buf=ftdi.read_data(context, 100)
    if ret>0:
        print read_buf[:ret],

    # send command
    ftdi.write_data(context, args.command, len(args.command))

    # main loop
    f.seek(512, SEEK_SET)
    while True:
        # read in data
        ret, read_buf=ftdi.read_data(context, 100)
        while ret>0:
            stdout.write(read_buf[:ret])
            ret, read_buf=ftdi.read_data(context, 100)
        # write data
        data=f.read(818400)
        if len(data)==0:
            break
        ftdi.write_data(context, data, len(data))
