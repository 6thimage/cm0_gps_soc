#! /usr/bin/env python

from Tkinter import Tk, BOTH, Text, StringVar, Menu
from tkFileDialog import askopenfilename
from ttk import Frame, Entry, Button, Scrollbar, Label
import ftdi1 as ftdi
from multiprocessing import Process, Pipe, Event
from threading import Thread
from Queue import Queue
import time
from os import SEEK_END, SEEK_SET

class window(Frame):
    def __init__(self, parent):
        # constants
        self.MSG_OUT=0
        self.MSG_IN=1
        self.MSG_INFO=2
        self.MSG_ERROR=3
        # init
        self.sector_skip=False
        self.file_increment=False
        Frame.__init__(self, parent)
        self.parent=parent
        self.msg_queue=Queue()
        self.init_ui()
    def init_ui(self):
        self.parent.title("FT2232 gui")
        #self.pack(fill=BOTH, expand=1)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        # create text window disabled
        self.scroll=Scrollbar(self)
        self.scroll.grid(stick='w', row=0, column=2, sticky='nsw')
        self.text=Text(self, width=50, height=20, state='disabled', yscrollcommand=self.scroll.set)
        self.text.grid(stick='w', row=0, column=0, columnspan=2, sticky='nsew')
        self.scroll.config(command=self.text.yview)
        self.text_entered=0
        self.text.tag_configure('in', foreground='red')
        self.text.tag_configure('out', foreground='green')
        self.text.tag_configure('info', foreground='grey')
        self.text.tag_configure('error', foreground='black')

        # right click menu for text window
        self.text_menu=Menu(self, tearoff=0)
        self.text_menu.add_command(label='copy', command=self.text_copy)
        self.text_menu.add_command(label='clear', command=self.text_clear)
        self.text.bind('<Button-3>', self.text_context_menu)

        # create entry text
        self.entry=Entry(self, width=50)
        self.entry.grid(stick='w', row=1, column=0, sticky='nsew')
        self.entry.bind('<Return>', self.send_button_event)
        self.entry.focus_set()

        # send button
        Button(self, text='>>', command=self.send_button).grid(row=1, column=1, sticky='nsew')

        # control frame
        self.controlframe=Frame(self)
        self.controlframe.grid(row=0, column=3)

        # file name
        Label(self.controlframe, text="File: ").grid(stick='nw', row=0, column=0)
        self.filename=StringVar()
        self.filename.set("none")
        Label(self.controlframe, textvar=self.filename, wraplength=180).grid(
                                                    stick='nw', row=0, column=1, columnspan=2)
        Button(self.controlframe, text="...", command=self.filename_open_button).grid(stick='e',
                                                                                      row=1,
                                                                                      column=2)

        Label(self.controlframe, text='').grid(row=3, column=0)

        # bytes to send
        Label(self.controlframe, text="Bytes to send: ").grid(row=4, column=0,
                                                                        columnspan=2)
        self.filesize=StringVar()
        self.filesize_entry=Entry(self.controlframe, textvariable=self.filesize, width=12)
        self.filesize_entry.grid(row=4, column=2)

        Label(self.controlframe, text='').grid(row=5, column=0)

        # menu for bytes to send
        self.filesize_menu=Menu(self, tearoff=0)
        self.filesize_menu.add_command(label="8184", command=self.filesize_8184)
        self.filesize_menu.add_command(label="8184 skip 512", command=self.filesize_8184_skip)
        self.filesize_menu.add_command(label="16368", command=self.filesize_16368)
        self.filesize_menu.add_command(label="16368 skip 512", command=self.filesize_16368_skip)
        self.filesize_menu.add_command(label="file", command=self.filesize_full)
        self.filesize_menu.add_command(label="file skip 512", command=self.filesize_full_skip)
        self.filesize_menu.add_command(label="incrementing 10s skip 512", command=self.filesize_increment)
        self.filesize_entry.bind('<Button-3>', self.filesize_context_menu)

        # send button
        Button(self.controlframe, text='Send file', command=self.file_send_button).grid(row=6,
                                                                                        column=2)

    def write(self, msg, whom):
        self.text['state']='normal'
        if not self.text_entered:
            self.text_entered=1
        else:
            self.text.insert('end', '\n')
        if whom==self.MSG_IN:
            self.text.insert('end', '>> '+msg, ('in'))
        elif whom==self.MSG_OUT:
            self.text.insert('end', '<< '+msg, ('out'))
            self.msg_queue.put(msg)
        elif whom==self.MSG_INFO:
            self.text.insert('end', '-- '+msg, ('info'))
        elif whom==self.MSG_ERROR:
            self.text.insert('end', '!! '+msg, ('error'))
        self.text['state']='disabled'
        self.text.yview('end')

    def send_button_event(self, event):
        self.send_button()
    def send_button(self):
        msg=self.entry.get()
        if len(msg):
            self.entry.delete(0, 'end')
            self.write(msg, 0)
        self.entry.focus_set()

    def text_context_menu(self, event):
        self.text_menu.tk_popup(event.x_root, event.y_root)

    def text_copy(self):
        self.clipboard_clear()
        text=self.text.get('1.0', 'end-1c')
        self.clipboard_append(text)

    def text_clear(self):
        self.text['state']='normal'
        self.text.delete('3.0', 'end')
        self.text['state']='disabled'

    def filesize_context_menu(self, event):
        self.filesize_menu.tk_popup(event.x_root, event.y_root)

    def filesize_full(self):
        self.sector_skip=False
        self.file_increment=False
        try:
            f=open(self.filename.get(), 'rb')
            with f:
                f.seek(0, SEEK_END)
                filesize=f.tell()
        except IOError:
            filesize='???'
        finally:
            self.filesize.set(filesize)

    def filesize_full_skip(self):
        self.sector_skip=True
        self.file_increment=False
        try:
            f=open(self.filename.get(), 'rb')
            with f:
                f.seek(0, SEEK_END)
                filesize=f.tell()-512
        except IOError:
            filesize='???'
        finally:
            self.filesize.set(filesize)

    def filesize_8184(self):
        self.filesize.set('8184')
        self.sector_skip=False
        self.file_increment=False

    def filesize_8184_skip(self):
        self.filesize.set('8184')
        self.sector_skip=True
        self.file_increment=False

    def filesize_16368(self):
        self.filesize.set('16368')
        self.sector_skip=False
        self.file_increment=False

    def filesize_16368_skip(self):
        self.filesize.set('16368')
        self.sector_skip=True
        self.file_increment=False

    def filesize_increment(self):
        self.filesize.set('81840000')
        self.sector_skip=True
        self.file_increment=True
        self.file_skip=512

    def filename_open_button(self):
        # get the current name
        filename=self.filename.get()
        # set to an empty string, if it is 'none'
        directory=''
        if filename=='none':
            directory='/media/ian/ext_gps_data/spirent_sim/'
            filename=''
        # open the dialog
        filename=askopenfilename(initialfile=filename,
                                 initialdir=directory,
                                 filetypes=[('all files', '.*')],
                                 title='Open data file')
        # if we have a filename, set it
        if filename!='':
            self.filename.set(filename)
            self.filesize_full()

    def file_send_button(self):
        filename=self.filename.get()
        try:
            filesize=int(self.filesize.get())
        except ValueError:
            filesize=self.filesize_full()
        try:
            with open(filename, 'rb') as f:
                if self.file_increment:
                    f.seek(self.file_skip, SEEK_SET)
                elif self.sector_skip:
                    f.seek(512, SEEK_SET)
                data=f.read(filesize)
                if self.file_increment:
                    self.file_skip=f.tell()
        except IOError:
            data=None
        finally:
            if data==None:
                self.write("Failed to open file '{}'".format(filename), self.MSG_INFO)
            else:
                self.write("Sending {} bytes skip {} from file '{}'".format(len(data),
                                        self.file_skip, filename), self.MSG_INFO)
            if len(data)>81840:
                # chunk-u-late data
                for i in range(0, len(data), 81840):
                    self.msg_queue.put(data[i:i+81840])
            else:
                self.msg_queue.put(data)

    def run(self, pipe, app_close):
        while not app_close.is_set():
            # poll pipe
            while pipe.poll():
                # get data - this will only be stuff being added to the text view
                data=pipe.recv()
                self.write(data[0], data[1])
            # poll queue
            counter=0
            while not self.msg_queue.empty() and counter<10:
                msg=self.msg_queue.get()
                pipe.send([msg])
                counter=counter+1
            if counter!=10:
                time.sleep(0.1)

class ft2232():
    def __init__(self):
        # constants
        self.MSG_OUT=0
        self.MSG_IN=1
        self.MSG_INFO=2
        self.MSG_ERROR=3

    def new(self, pipe):
        self.context=ftdi.new()
        pipe.send(['libftdi {}'.format(ftdi.get_library_version().version_str), self.MSG_INFO])

    def open(self, pipe):
        ret, dev_list=ftdi.usb_find_all(self.context, 0, 0)
        if ret<0:
            self.isopen=0
            return False
        if not dev_list:
            pipe.send(['no devices found', self.MSG_INFO])
            self.isopen=0
            return False
        node=dev_list
        while node:
            ret, manu, desc, ser=ftdi.usb_get_strings(self.context, node.dev)
            if ret>=0:
                pipe.send(['{} {} {}'.format(manu, desc, ser), self.MSG_INFO])
            node=node.next

        # open first device
        ftdi.set_interface(self.context, ftdi.INTERFACE_B)
        ret=ftdi.usb_open_dev(self.context, dev_list.dev)
        ftdi.list_free2(dev_list)
        if ret<0:
            self.isopen=0
            return False

        # set mode
        ret=ftdi.set_bitmode(self.context, 0xff, ftdi.BITMODE_SYNCFF)
        if ret<0:
            self.isopen=0
            return False

        self.isopen=1
        return True

    def run(self, pipe, app_close):
        self.new(pipe)
        self.open(pipe)
        buf=''
        while not app_close.is_set():
            # poll ftdi
            if self.isopen:
                ret, read_buf=ftdi.read_data(self.context, 100)
                if ret>0:
                    # combine buffers
                    buf+=read_buf[:ret]
                    # split into lines
                    nl=buf.find('\n')
                    while nl!=-1:
                        # send line
                        pipe.send([buf[:nl], self.MSG_IN])
                        # remove line from buffer
                        buf=buf[nl+1:]
                        # update condition
                        nl=buf.find('\n')
            # poll pipe
            counter=0
            while pipe.poll() and counter<10:
                data=pipe.recv()
                if not self.isopen:
                    pipe.send(['Device not open', self.MSG_ERROR])
                else:
                    ftdi.write_data(self.context, data[0], len(data[0]))
                counter=counter+1
            if counter!=10:
                time.sleep(0.1)

    def free(self):
        if self.context:
            ftdi.free(self.context)
            self.context=None

if __name__ == '__main__':
    # create window
    root=Tk()
    root.resizable(True, True)
    root.geometry('800x600+150+150')
    root.minsize(400,300)
    root.grid_columnconfigure(0, weight=1)
    root.grid_rowconfigure(0, weight=1)
    app=window(root)
    app.grid(sticky='nswe')

    # create ft2232
    ft=ft2232()

    # pipe for inter-process messages
    pipe_a, pipe_b=Pipe()
    # event to signal when to exit the thread and process
    app_close=Event()
    # create process and thread
    # thread used so it can update the gui
    ft_proc=Process(target=ft.run, args=(pipe_a, app_close))
    win_proc=Thread(target=app.run, args=(pipe_b, app_close))

    # start processes
    ft_proc.start()
    win_proc.start()

    # do gui loop
    root.mainloop()

    # set close
    app_close.set()
    # wait for process & thread to exit
    ft_proc.join()
    win_proc.join()
