#! /usr/bin/env python

from struct import unpack

with open('image.bin', 'rb') as f:
    with open('image.mem', 'w') as w:
        d = f.read(4)
        counter = 0
        w.write('@00\n')
        while d:
            d=unpack('HH',d)
            w.write('{:04x}{:04x}\n'.format(d[1], d[0]))
            d = f.read(4)
