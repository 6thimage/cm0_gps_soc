#ifndef _FFT_H_
#define _FFT_H_

#include "cm0.h"

#define FFT_BIT_STATUS_IDLE        (0x1)
#define FFT_BIT_STATUS_ENABLE      (0x2)

#define FFT_idle()         (FFT->status & FFT_BIT_STATUS_IDLE)
#define FFT_enable()       (FFT->status |= FFT_BIT_STATUS_ENABLE)
#define FFT_disable()      (FFT->status &= ~FFT_BIT_STATUS_ENABLE)

#endif /* !_FFT_H_ */
