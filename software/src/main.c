#include "cm0.h"
#include "gpio.h"
#include "ft245.h"
#include "fft.h"
#include "channel.h"
#include "channelgroup.h"
#include "baseband.h"
#include "soc_con.h"

#include "sm_printf.h"

uint32_t systick_ms=0, code_req_timer=0, code_requested=0;

void SysTick_Handler(void)
{
    ++systick_ms;
    ++code_req_timer;

    if(code_req_timer==1001)
    {
        code_req_timer=0;
        code_requested=1;
        ChannelGroup_code_request();
    }
}

typedef struct
{
    int8_t doppler;
    uint16_t peak_location;
    uint32_t snr;
} result_entry;

result_entry results[32];

void result_clear()
{
    uint32_t i;

    for(i=0; i<32; ++i)
    {
        results[i].doppler=0;
        results[i].peak_location=0;
        results[i].snr=0;
    }
}

int main(void)
{
    uint32_t i;

    sm_printf("Firmware build: " __DATE__ " " __TIME__ "\n");

    /* enable systick */
    SysTick_Config(50000000/1000);

    /* enable FFT interrupt */
    NVIC_EnableIRQ(FFT_IRQn);
    FFT->interrupt_enable=1;

    /* enable channel interrupt */
    NVIC_EnableIRQ(ChannelGroup_IRQn);
    ChannelGroup->interrupt_enable=1;

    /* setup FFT */
    FFT->centre_freq=4096; /* 4.092 MHz - 4092*1024/1023 (ratio of FFT to code length) */
    FFT->doppler_side_shift=10; /* ~ 10 kHz */
    FFT->satellites=0xffffffff; /* all 32 satellites */

    /* set baseband mux to FT245 */
    SoC_Con_baseband_mode_FT245();

    sm_printf("centre freq: %d   doppler side shift: %d   satellites: %#x\n",
              FFT->centre_freq, FFT->doppler_side_shift, FFT->satellites);

    while(1)
    {
        char c;

        while(FT245_read_empty())
        {
            if(code_requested)
            {
                uint32_t i, channels;

                code_requested=0;

                channels=ChannelGroup->valid_channels;

                for(i=0; i<32; ++i)
                {
                    if(channels&0x1)
                    {
                        sm_printf("    TR prn %2d P %d O %d tow %5d code_top %u code_bottom %u time %u\n",
                                Channel(i)->PRN+1,
                                Channel_is_locked_pess(i),
                                Channel_is_locked_optim(i),
                                ChannelGroupChannel(i)->TOW,
                                ChannelGroupChannel(i)->code_num_top,
                                ChannelGroupChannel(i)->code_num_btm,
                                ChannelGroupChannel(i)->channel_time);
                    }
                    channels>>=1;
                }
            }
        }

        SoC_Con_FT245_disable();

        c=FT245->read;

        if(c=='a')
        {
            FFT_enable();
            SoC_Con->ft245_count=8184;
            SoC_Con_FT245_mode_count();
            SoC_Con_FT245_enable();
        }
        else if(c=='b')
        {
            result_clear();
            FFT_enable();
            while(!FFT_idle()) ;
            for(i=0; i<32; ++i)
            {
                sm_printf("tick: %8d sat: %2d dopp: % 3d peak_loc: %5d snr: %3d.%03d\n",
                            systick_ms, i+1, results[i].doppler, results[i].peak_location,
                            results[i].snr/1000, results[i].snr%1000);
            }
        }
        else if(c=='c')
        {
            result_clear();
        }
        else if(c=='s')
        {
            sm_printf("fft status: %#x\n", FFT->status);
            sm_printf("baseband status: %#x\n", Baseband->status);
        }
        else if(c=='r')
        {
            NVIC_SystemReset();
        }
        else if(c=='p')
        {
            for(i=0; i<32; ++i)
            {
                sm_printf("tick: %8d sat: %2d dopp: % 3d peak_loc: %5d snr: %3d.%03d\n",
                            systick_ms, i+1, results[i].doppler, results[i].peak_location,
                            results[i].snr/1000, results[i].snr%1000);
            }
        }
        else if(c=='o')
        {
            uint64_t code_step;
            ChannelGroup_baseband_mode_3bit();
            ChannelGroup->carrier_c1=0x49;
            ChannelGroup->carrier_c2=-0x47;
            ChannelGroup->code_c1=0xadde71;
            ChannelGroup->code_c2=-0xada255;
            /* setup channel 0 */
            Channel(0)->PRN=30-1;
            Channel(0)->carrier_theta=0;
            Channel(0)->carrier_step=((4.092e6 + 794.)/(16.368e6))*((float)(1<<30));
            Channel(0)->code_number=1023-374;
            code_step=((1.023e6 + (794./1540.))/(16.368e6))*((float)(1ULL<<36));
            Channel(0)->code_step_top=Channel_code_step_top(code_step);
            Channel(0)->code_step_bottom=Channel_code_step_bottom(code_step);
            Channel_enable(0);
            Channel_configure(0);
            /* setup channel 1 */
            Channel(1)->PRN=5-1;
            Channel(1)->carrier_theta=0;
            Channel(1)->carrier_step=((4.092e6 + 672.)/(16.368e6))*((float)(1<<30));
            Channel(1)->code_number=1023-635;
            code_step=((1.023e6 + (672./1540.))/(16.368e6))*((float)(1ULL<<36));
            Channel(1)->code_step_top=Channel_code_step_top(code_step);
            Channel(1)->code_step_bottom=Channel_code_step_bottom(code_step);
            Channel_enable(1);
            Channel_configure(1);
            /* setup channel 2 */
            Channel(2)->PRN=20-1;
            Channel(2)->carrier_theta=0;
            Channel(2)->carrier_step=((4.092e6 + 3482.)/(16.368e6))*((float)(1<<30));
            Channel(2)->code_number=1023-77;
            code_step=((1.023e6 + (3482./1540.))/(16.368e6))*((float)(1ULL<<36));
            Channel(2)->code_step_top=Channel_code_step_top(code_step);
            Channel(2)->code_step_bottom=Channel_code_step_bottom(code_step);
            Channel_enable(2);
            Channel_configure(2);
            /* setup channel 3 */
            Channel(3)->PRN=15-1;
            Channel(3)->carrier_theta=0;
            Channel(3)->carrier_step=((4.092e6 + 3727.)/(16.368e6))*((float)(1<<30));
            Channel(3)->code_number=1023-397;
            code_step=((1.023e6 + (3727./1540.))/(16.368e6))*((float)(1ULL<<36));
            Channel(3)->code_step_top=Channel_code_step_top(code_step);
            Channel(3)->code_step_bottom=Channel_code_step_bottom(code_step);
            Channel_enable(3);
            Channel_configure(3);

            //while(!Channel_is_active(0)) ;
            //sm_printf("tick %d channel active\n", systick_ms);

            //while(!Channel_is_running(0)) ;
            //sm_printf("tick %d channel running\n", systick_ms);
        }
        else if(c=='z')
        {
            /* use SD card for baseband */
            SoC_Con_baseband_mode_SD();
            Baseband->start_block=1;
            Baseband->num_blocks=29300000;
            Baseband_enable();
            Baseband_prevent_restart();
            Baseband_enable_playback();
        }
        else
            sm_printf("%c\n", c);
    }
}

void sm_write(int ch)
{
    while(FT245_write_full()) ;
    FT245->write=ch;
}

/* FFT interrupt handler */
void FFT_Handler(void)
{
    uint32_t sat_num, sat_peak, sat_mean, snr;

    FFT->interrupt_flags=0;

    sat_num=FFT->sat_num;
    sat_peak=FFT->sat_peak;
    sat_mean=FFT->sat_mean;

    /* if overflow ignore result */
    if(sat_mean>>17)
        return;

    /* calculate snr */
    snr=(sat_peak*1000*64);
    /* only divide by mean if it is non-zero */
    if(sat_mean!=0)
        snr/=sat_mean;

    /* store if better */
    if(snr > results[sat_num].snr)
    {
        results[sat_num].doppler=FFT->sat_doppler;
        results[sat_num].peak_location=FFT->sat_peak_loc;
        results[sat_num].snr=snr;
    }
}

/* channel group interrupt handler */
void ChannelGroup_Handler(void)
{
    uint32_t i, channels=ChannelGroup->interrupt_flags;

    ChannelGroup->interrupt_flags=0;

    for(i=0; i<32; ++i)
    {
        if(channels&0x1)
        {
            /* print the time of week */
            sm_printf("tick: %d sat: %d TOW: %u alert: %d enhanced: %d\n",
                    systick_ms, Channel(i)->PRN+1, Channel(i)->TOW,
                    Channel_alert_flag_set(i)?1:0, Channel_enhanced_flag_set(i)?1:0);

            sm_printf(">%d %08x\n", i, Channel(i)->decoder_status);

            if(Channel_is_frame1_valid(i))
            {
                sm_printf("  F1: week num: %u IODC: %u URA index: %u SV health: %d (%u)\n",
                          Channel(i)->week_num, Channel(i)->IODC, Channel(i)->URA_index,
                          Channel(i)->SV_health_bad?1:0, Channel(i)->SV_health);
                sm_printf("      t_oc: %u t_GD: %d a_f2: %d a_f1: %d a_f0: %d\n",
                          Channel(i)->t_oc, Channel(i)->t_GD, Channel(i)->a_f2, Channel(i)->a_f1,
                          Channel(i)->a_f0);
            }
            if(Channel_is_frame2_valid(i))
            {
                sm_printf("  F2: IODE: %u curve fit %c 4 hours\n",
                          Channel(i)->IODE_sf2, Channel(i)->curve_fit_greater?'>':'=');
                sm_printf("      M_0: %d ecc: %d sqrt_A: %u Delta_n: %d\n",
                          Channel(i)->M_0, Channel(i)->ecc, Channel(i)->sqrt_A, Channel(i)->Delta_n);
                sm_printf("      t_oe: %d C_rs: %d C_uc: %d C_us: %d\n",
                          Channel(i)->t_oe, Channel(i)->C_rs, Channel(i)->C_uc, Channel(i)->C_us);
            }
            if(Channel_is_frame3_valid(i))
            {
                sm_printf("  F3: IODE: %d\n", Channel(i)->IODE_sf3);
                sm_printf("      Omega_0: %d incl_0: %d C_rc: %d C_ic: %d\n",
                          Channel(i)->Omega_0, Channel(i)->incl_0, Channel(i)->C_rc, Channel(i)->C_ic);
                sm_printf("      C_is: %d omega: %d Omega^dot: %d IDOT: %d\n",
                          Channel(i)->C_is, Channel(i)->omega, Channel(i)->Omega_dot, Channel(i)->IDOT);
            }
        }

        channels>>=1;
    }
}

void HardFault_Handler(uint32_t *sp)
{
    sm_printf("hardfault handler hit\n");

    sm_printf("sp: 0x%08x\n", sp);

    sm_printf("r0:  0x%08x  r1: 0x%08x  r2: 0x%08x   r3: 0x%08x\n"
              "r12: 0x%08x  lr: 0x%08x  pc: 0x%08x  psr: 0x%08x\n",
              sp[0], sp[1], sp[2], sp[3],
              sp[4], sp[5], sp[6], sp[7]);

    sm_printf("fault status\n"
              "    config: 0x%08x  hard: 0x%0x%08x\n"
              "    debug:  0x%08x  aux:  0x%0x%08x\n"
              "fault address: 0x%08x\n",
              *((uint32_t*)0xE000ED28), *((uint32_t*)0xE000ED2c),
              *((uint32_t*)0xE000ED30), *((uint32_t*)0xE000ED3c),
              *((uint32_t*)0xE000ED38));

    while(1) ;
}

