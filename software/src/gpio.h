#ifndef _GPIO_H_
#define _GPIO_H_

#include <stdint.h>

#define GPIO_PIN_0  ((uint32_t)0x00000001)
#define GPIO_PIN_1  ((uint32_t)0x00000002)
#define GPIO_PIN_2  ((uint32_t)0x00000004)
#define GPIO_PIN_3  ((uint32_t)0x00000008)
#define GPIO_PIN_4  ((uint32_t)0x00000010)
#define GPIO_PIN_5  ((uint32_t)0x00000020)
#define GPIO_PIN_6  ((uint32_t)0x00000040)
#define GPIO_PIN_7  ((uint32_t)0x00000080)
#define GPIO_PIN_8  ((uint32_t)0x00000100)
#define GPIO_PIN_9  ((uint32_t)0x00000200)
#define GPIO_PIN_10 ((uint32_t)0x00000400)
#define GPIO_PIN_11 ((uint32_t)0x00000800)
#define GPIO_PIN_12 ((uint32_t)0x00001000)
#define GPIO_PIN_13 ((uint32_t)0x00002000)
#define GPIO_PIN_14 ((uint32_t)0x00004000)
#define GPIO_PIN_15 ((uint32_t)0x00008000)

#endif /* !_GPIO_H_ */
