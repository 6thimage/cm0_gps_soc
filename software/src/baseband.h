#ifndef _BASEBAND_H_
#define _BASEBAND_H_

#include "cm0.h"

#define Baseband_STATUS_BIT_ACTIVE          (0x01)
#define Baseband_STATUS_BIT_ENABLE_PLAYBACK (0x02)
#define Baseband_STATUS_BIT_ALLOW_RESTART   (0x04)
#define Baseband_STATUS_BIT_IDLE            (0x08)
#define Baseband_STATUS_BIT_ERROR           (0x10)

#define Baseband_enable()           (Baseband->status |= Baseband_STATUS_BIT_ACTIVE)
#define Baseband_disable()          (Baseband->status &= ~Baseband_STATUS_BIT_ACTIVE)
#define Baseband_enable_playback()  (Baseband->status |= Baseband_STATUS_BIT_ENABLE_PLAYBACK)
#define Baseband_disable_playback() (Baseband->status &= ~Baseband_STATUS_BIT_ENABLE_PLAYBACK)
#define Baseband_allow_restart()    (Baseband->status |= Baseband_STATUS_BIT_ALLOW_RESTART)
#define Baseband_prevent_restart()  (Baseband->status &= ~Baseband_STATUS_BIT_ALLOW_RESTART)

#define Baseband_is_idle()      (Baseband->status & Baseband_STATUS_BIT_IDLE)
#define Baseband_is_error_set() (Baseband->status & Baseband_STATUS_BIT_ERROR)

#endif /* ! _BASEBAND_H_ */
