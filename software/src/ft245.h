#ifndef _FT245_H_
#define _FT245_H_

#include "cm0.h"

#define FT245_BIT_READ_EMPTY (0x1)
#define FT245_BIT_WRITE_FULL (0x2)

#define FT245_read_empty()  (FT245->status & FT245_BIT_READ_EMPTY)
#define FT245_write_full()  (FT245->status & FT245_BIT_WRITE_FULL)

#endif /* !_FT245_H_ */

