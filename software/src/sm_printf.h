#ifndef _SM_PRINTF_H
#define _SM_PRINTF_H

#include <stddef.h>

/* implementation defined functions */
void sm_write(int ch);

typedef void (*sm_putc_func)(int);

/* library functions */
int sm_printf(const char *format, ...);
int sm_snprintf(char *str, size_t size, const char *format, ...);

#endif /* !_SM_PRINTF_H */
