#ifndef _SOC_CON_H_
#define _SOC_CON_H_

#include "cm0.h"

#define SoC_Con_FT245_BIT_ENABLE (0x1)
#define SoC_Con_FT245_BIT_MODE   (0x2)

#define SoC_Con_FT245_enable()     (SoC_Con->ft245_control |= SoC_Con_FT245_BIT_ENABLE)
#define SoC_Con_FT245_disable()    (SoC_Con->ft245_control &= ~SoC_Con_FT245_BIT_ENABLE)

#define SoC_Con_FT245_mode_mux()   (SoC_Con->ft245_control &= ~SoC_Con_FT245_BIT_MODE)
#define SoC_Con_FT245_mode_count() (SoC_Con->ft245_control |= SoC_Con_FT245_BIT_MODE)

#define SoC_Con_BASEBAND_MODE_SD    (0)
#define SoC_Con_BASEBAND_MODE_FT245 (1)

#define SoC_Con_baseband_mode_SD()    (SoC_Con->baseband_mode = SoC_Con_BASEBAND_MODE_SD)
#define SoC_Con_baseband_mode_FT245() (SoC_Con->baseband_mode = SoC_Con_BASEBAND_MODE_FT245)

#endif /* !_SOC_CON_H_ */

