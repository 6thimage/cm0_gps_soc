#ifndef _CHANNELGROUP_H_
#define _CHANNELGROUP_H_

#include "cm0.h"

#define ChannelGroup_BASEBAND_MODE_2BIT (0)
#define ChannelGroup_BASEBAND_MODE_3BIT (1)

#define ChannelGroup_code_request()         (ChannelGroup->code_num_request = 1)
#define ChannelGroup_baseband_mode_2bit()   (ChannelGroup->baseband_mode = ChannelGroup_BASEBAND_MODE_2BIT)
#define ChannelGroup_baseband_mode_3bit()   (ChannelGroup->baseband_mode = ChannelGroup_BASEBAND_MODE_3BIT)

#define ChannelGroup_int_enable(n)  (ChannelGroup->interrupt_enable |= (1<<(n)))
#define ChannelGroup_int_disable(n) (ChannelGroup->interrupt_enable &= ~(1<<(n)))

#endif /* ! _CHANNELGROUP_H_ */
