#ifndef _CM0_H_
#define _CM0_H_

#include <stdint.h>

/* interrupts */
typedef enum IRQn
{
/* Cortex-M0 Processor Exceptions Numbers */
  NonMaskableInt_IRQn           = -14, /* Non Maskable Interrupt */
  HardFault_IRQn                = -13, /* Hard Fault Interrupt */
  SVCall_IRQn                   = -5,  /* SV Call Interrupt */
  PendSV_IRQn                   = -2,  /* Pend SV Interrupt */
  SysTick_IRQn                  = -1,  /* System Tick Interrupt */
/* Device Specific Interrupt Numbers */
  FFT_IRQn                      = 0,   /* FFT Interrupt */
  ChannelGroup_IRQn             = 1,   /* Channel group interrupt */
} IRQn_Type;

/* Configuration of the Cortex-M0 Processor and Core Peripherals */
#define __CM0_REV                 0x0000    /* Core Revision r0p0 */
#define __NVIC_PRIO_BITS          2         /* Number of Bits used for Priority Levels */
#define __Vendor_SysTickConfig    0         /* Set to 1 if different SysTick Config is used */
#define __MPU_PRESENT             0         /* MPU present or not  */

#include "CMSIS/core_cm0.h"

#define PERIPH_BASE ((uint32_t) 0x40000000)

#define GPIO_BASE                   (PERIPH_BASE + 0x0000000)
#define FT245_BASE                  (PERIPH_BASE + 0x1000000)
#define FFT_BASE                    (PERIPH_BASE + 0x2000000)
#define CHANNELGROUP_BASE           (PERIPH_BASE + 0x4000000)
#define CHANNELGROUPCHANNEL_BASE    (CHANNELGROUP_BASE + 0x20)
#define CHANNEL_BASE                (PERIPH_BASE + 0x5000000)
#define BASEBAND_BASE               (PERIPH_BASE + 0x6000000)
#define SOC_CON_BASE                (PERIPH_BASE + 0x8000000)

#define CHANNEL_SEP                 (0x100)
#define CHANNELGROUPCHANNEL_SEP     (0x20)

/* GPIO */
typedef struct
{
    /* 0x00 GPIO direction */
    volatile uint32_t dir;
    /* 0x04 GPIO data */
    volatile uint32_t data;
} GPIO_TypeDef;
#define GPIO ((GPIO_TypeDef *) GPIO_BASE)

/* FT245 */
typedef struct
{
    /* 0x00 status register */
    volatile uint32_t status:8;
    uint32_t reserved0:24;
    /* 0x04 write fifo */
    volatile uint32_t write:8;
    uint32_t reserved1:24;
    /* 0x08 read fifo */
    volatile uint32_t read:8;
    uint32_t reserved2:24;

} FT245_TypeDef;
#define FT245 ((FT245_TypeDef *) FT245_BASE)

/* FFT */
typedef struct
{
    /* 0x00 status register */
    volatile uint32_t status:2;
    uint32_t reserved0:30;
    /* 0x04 frequency register */
    volatile uint32_t centre_freq:14;
    uint32_t reserved1:10;
    volatile uint32_t doppler_side_shift:6;
    uint32_t reserved2:2;
    /* 0x08 satellite register */
    volatile uint32_t satellites;
    /* 0x0c reserved */
    uint32_t reserved3;
    /* 0x10 interrupt enable */
    volatile uint32_t interrupt_enable:1;
    uint32_t reserved4:31;
    /* 0x14 interrupt flag */
    volatile uint32_t interrupt_flags:1;
    uint32_t reserved5:31;
    /* 0x18 reserved */
    uint32_t reserved6;
    /* 0x1c reserved */
    uint32_t reserved7;
    /* 0x20 satellite number and doppler */
    volatile uint32_t sat_num:5;
    uint32_t reserved8:3;
    volatile int32_t sat_doppler:7;
    uint32_t reserved9:17;
    /* 0x24 satellite peak */
    volatile uint32_t sat_peak:18;
    volatile uint32_t sat_peak_loc:14;
    /* 0x28 satellite mean */
    volatile uint32_t sat_mean:18;
    uint32_t reserved10:14;
} FFT_TypeDef;
#define FFT ((FFT_TypeDef *) FFT_BASE)

/* channel group */
typedef struct
{
    /* 0x00 control */
    volatile uint32_t code_num_request:1;
    volatile uint32_t baseband_mode:1;
    uint32_t reserved0:30;
    /* 0x04 carrier constants */
    volatile int32_t carrier_c1:16;
    volatile int32_t carrier_c2:16;
    /* 0x08 code constant 1 */
    volatile int32_t code_c1:26;
    uint32_t reserved1:6;
    /* 0x0c code constant 2 */
    volatile int32_t code_c2:26;
    uint32_t reserved2:6;
    /* 0x10 interrupt enable */
    volatile uint32_t interrupt_enable:32;
    /* 0x14 interrupt flags */
    volatile uint32_t interrupt_flags:32;
    /* 0x18 valid channels */
    volatile uint32_t valid_channels:32;
} ChannelGroup_TypeDef;
#define ChannelGroup ((ChannelGroup_TypeDef *) CHANNELGROUP_BASE)

typedef struct
{
    /* 0x00 TOW */
    volatile uint32_t TOW:17;
    uint32_t reserved0:15;
    /* 0x04 IOD */
    volatile uint32_t IOD:10;
    uint32_t reserved1:22;
    /* 0x08 code number [45:14] */
    volatile uint32_t code_num_top:32;
    /* 0x0c code number [13:0] */
    volatile uint32_t code_num_btm:14;
    uint32_t reserved2:18;
    /* 0x10 channel time */
    volatile uint32_t channel_time:27;
    uint32_t reserved3:5;
} ChannelGroupChannel_TypeDef;
#define ChannelGroupChannel(n) ((ChannelGroupChannel_TypeDef *) (((n)*CHANNELGROUPCHANNEL_SEP) + CHANNELGROUPCHANNEL_BASE))

/* channel */
typedef struct
{
    /* 0x00 status */
    volatile uint32_t status:5;
    uint32_t reserved0:27;
    /* 0x04 correlator */
    volatile uint32_t PRN:5;
    uint32_t reserved1:27;
    /* 0x08 carrier NCO 1 */
    volatile uint32_t carrier_theta:30;
    uint32_t reserved2:2;
    /* 0x0c carrier NCO 2 */
    volatile uint32_t carrier_step:30;
    uint32_t reserved3:2;
    /* 0x10 code NCO 1 */
    volatile uint32_t code_number:10;
    volatile uint32_t code_step_top:4;
    uint32_t reserved4:18;
    /* 0x14 code NCO 2 */
    volatile uint32_t code_step_bottom:32;
    /* 0x18 decoder */
    volatile uint32_t decoder_status:6;
    uint32_t reserved5:9;
    volatile uint32_t TOW:17;
    /* 0x1c subframe 1a */
    volatile uint32_t IODC:10;
    volatile uint32_t SV_health_bad:1;
    volatile uint32_t SV_health:5;
    volatile uint32_t week_num:10;
    uint32_t reserved6:6;
    /* 0x20 subframe 1b */
    volatile uint32_t t_oc:16;
    volatile int32_t t_GD:8;
    volatile uint32_t URA_index:4;
    uint32_t reserved7:4;
    /* 0x24 subframe 1c */
    volatile int32_t a_f0:22;
    uint32_t reserved8:10;
    /* 0x24 subframe 1d */
    volatile int32_t a_f1:16;
    volatile int32_t a_f2:8;
    uint32_t reserved9:8;
    /* 0x28 subframe 2a */
    volatile uint32_t IODE_sf2:8;
    volatile uint32_t curve_fit_greater:1;
    uint32_t reserved10:7;
    volatile int32_t Delta_n:16;
    /* 0x2c subframe 2b */
    volatile int32_t M_0:32;
    /* 0x30 subframe 2c */
    volatile int32_t ecc:32;
    /* 0x34 subframe 2d */
    volatile uint32_t sqrt_A:32;
    /* 0x38 subframe 2e */
    volatile int32_t C_rs:16;
    volatile int32_t t_oe:16;
    /* 0x3c subframe 2f */
    volatile int32_t C_us:16;
    volatile int32_t C_uc:16;
    /* 0x40 subframe 3a */
    volatile uint32_t IODE_sf3:8;
    uint32_t reserved11:8;
    volatile int32_t IDOT:14;
    uint32_t reserved12:2;
    /* 0x44 subframe 3b */
    volatile int32_t Omega_0:32;
    /* 0x48 subframe 3c */
    volatile int32_t incl_0:32;
    /* 0x4c subframe 3d */
    volatile int32_t omega:32;
    /* 0x50 subframe 3e */
    volatile int32_t Omega_dot:24;
    uint32_t reserved13:8;
    /* 0x54 subframe 3f */
    volatile int32_t C_rc:16;
    uint32_t reserved14:16;
    /* 0x58 subframe 3g */
    volatile int32_t C_is:16;
    volatile int32_t C_ic:16;
} Channel_TypeDef;
#define Channel(n) ((Channel_TypeDef *) (((n)*CHANNEL_SEP) + CHANNEL_BASE))

/* baseband */
typedef struct
{
    /* 0x00 status */
    volatile uint32_t status:5;
    uint32_t reserved0:27;
    /* 0x04 start block */
    volatile uint32_t start_block:32;
    /* 0x08 number of blocks */
    volatile uint32_t num_blocks:32;
} Baseband_TypeDef;
#define Baseband ((Baseband_TypeDef *) BASEBAND_BASE)

/* SoC control */
typedef struct
{
    /* 0x00 FT245 fifo */
    volatile uint32_t ft245_control:8;
    uint32_t reserved0:8;
    volatile uint32_t ft245_count:16;
    /* 0x04 baseband */
    volatile uint32_t baseband_mode:1;
    uint32_t reserved1:31;
} SoC_Con_TypeDef;
#define SoC_Con ((SoC_Con_TypeDef *) SOC_CON_BASE)

#endif /* !_CM0_H_ */
