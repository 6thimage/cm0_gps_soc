  .syntax unified
  .arch armv6-m
  .thumb

.global g_isr_vectors
.global Default_Handler

  .section .text.Reset_Handler
  .globl Reset_Handler
  .type Reset_Handler, %function
Reset_Handler:
  /* text copy - currently not needed, as running from ram */
//  ldr r1, =__etext
//  ldr r2, =__data_start
//  ldr r3, =__data_end
//
//  subs r3, r2
//  ble .text_done
//
//.text_loop:
//  subs r3, #4
//  ldr r0, [r1, r3]
//  str r0, [r2, r3]
//  bgt .test_loop

  /* clear bss */
  ldr r1, =__bss_start__
  ldr r2, =__bss_end__

  movs r0, 0
  subs r2, r1
  ble .bss_done

.bss_loop:
  subs r2, #4
  str r0, [r1, r2]
  bgt .bss_loop
.bss_done:

  /* call application entry point */
  bl main
.size Reset_Handler, .-Reset_Handler

/* default handler */
  .section .text.Default_Handler,"ax",%progbits
Default_Handler:
inf_loop:
  b inf_loop
  .size Default_Handler, .-Default_Handler

/* hardfault handler */
  .section .text.HardFault_Handler_asm,"ax",%progbits
HardFault_Handler_asm:
  movs r0, #4
  mov r1, lr
  tst r0, r1
  beq l_stack_msp
  mrs r0, psp
  b l_run_handler
l_stack_msp:
  mrs r0, msp
l_run_handler:
  ldr r1, =HardFault_Handler
  bx r1
  .size HardFault_Handler_asm, .-HardFault_Handler_asm

/* vector table */
  .section .isr_vector,"a",%progbits
  .type g_isr_vectors, %object
  .size g_isr_vectors, .-g_isr_vectors

g_isr_vectors:
  .word _estack
  .word Reset_Handler
  .word NMI_Handler
  .word HardFault_Handler
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0
  .word SVC_Handler
  .word 0
  .word 0
  .word PendSV_Handler
  .word SysTick_Handler
  /* external interrupts */
  .word FFT_Handler
  .word ChannelGroup_Handler
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0
  .word 0

  .weak NMI_Handler
  .thumb_set NMI_Handler, Default_Handler

  .weak HardFault_Handler
  .thumb_set HardFault_Handler, Default_Handler

  .weak SVC_Handler
  .thumb_set SVC_Handler, Default_Handler

  .weak PendSV_Handler
  .thumb_set PendSV_Handler, Default_Handler

  .weak SysTick_Handler
  .thumb_set SysTick_Handler, Default_Handler

  .weak FFT_Handler
  .thumb_set FFT_Handler, Default_Handler

  .weak ChannelGroup_Handler
  .thumb_set ChannelGroup_Handler, Default_Handler
