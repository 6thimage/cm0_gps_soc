#ifndef _CHANNEL_H_
#define _CHANNEL_H_

#include "cm0.h"

#define Channel_STATUS_BIT_ACTIVE               (0x01)
#define Channel_STATUS_BIT_CORRELATOR_RUNNING   (0x02)
#define Channel_STATUS_BIT_CORRELATOR_CONFIGURE (0x04)
#define Channel_STATUS_BIT_OPTIMISTIC_LOCK      (0x08)
#define Channel_STATUS_BIT_PESSIMISTIC_LOCK     (0x10)

#define Channel_enable(n)       (Channel(n)->status |= Channel_STATUS_BIT_ACTIVE)
#define Channel_disable(n)      (Channel(n)->status &= ~Channel_STATUS_BIT_ACTIVE)
#define Channel_configure(n)    (Channel(n)->status |= Channel_STATUS_BIT_CORRELATOR_CONFIGURE)

#define Channel_is_active(n)        (Channel(n)->status & Channel_STATUS_BIT_ACTIVE)
#define Channel_is_running(n)       (Channel(n)->status & Channel_STATUS_BIT_CORRELATOR_RUNNING)
#define Channel_is_locked_optim(n)  (Channel(n)->status & Channel_STATUS_BIT_OPTIMISTIC_LOCK)
#define Channel_is_locked_pess(n)   (Channel(n)->status & Channel_STATUS_BIT_PESSIMISTIC_LOCK)

#define Channel_DECODER_BIT_FRAME_READY     (0x01)
#define Channel_DECODER_BIT_FRAME_VALID1    (0x02)
#define Channel_DECODER_BIT_FRAME_VALID2    (0x04)
#define Channel_DECODER_BIT_FRAME_VALID3    (0x08)
#define Channel_DECODER_BIT_ALERT_FLAG      (0x10)
#define Channel_DECODER_BIT_ENHANCED_FLAG   (0x20)

#define Channel_is_frame_ready(n)       (Channel(n)->decoder_status & Channel_DECODER_BIT_FRAME_READY)
#define Channel_is_frame1_valid(n)      (Channel(n)->decoder_status & Channel_DECODER_BIT_FRAME_VALID1)
#define Channel_is_frame2_valid(n)      (Channel(n)->decoder_status & Channel_DECODER_BIT_FRAME_VALID2)
#define Channel_is_frame3_valid(n)      (Channel(n)->decoder_status & Channel_DECODER_BIT_FRAME_VALID3)
#define Channel_alert_flag_set(n)       (Channel(n)->decoder_status & Channel_DECODER_BIT_ALERT_FLAG)
#define Channel_enhanced_flag_set(n)    (Channel(n)->decoder_status & Channel_DECODER_BIT_ENHANCED_FLAG)

#define Channel_code_step_top(x)    (((x)>>32)&0xf)
#define Channel_code_step_bottom(x) ((x)&0xffffffff)

#endif /* ! _CHANNEL_H_ */
