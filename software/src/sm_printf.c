#include <stdarg.h>
#include <stdbool.h>

#include "sm_printf.h"

/* need to provide 
 *
 * void sm_write(char)      write char to output
 */

/* feature enables */
#define SM_SNPRINTF         0

#define SM_OCTAL            0
#define SM_BINARY           0
#define SM_FLOAT            0 /* currently not supported */
#define SM_WRITTEN_SO_FAR   0

#define SM_BIT_SHIFTS       0
#define SM_GT_HEX           0 /* enables support for bases >16 in sm_nutoa */

#define FLAG_ALT_FORM   (1<<0)
#define FLAG_PAD_ZERO   (1<<1)
#define FLAG_PAD_SPACE  (1<<2)
#define FLAG_INC_PLUS   (1<<3)
#define FLAG_LEFT_ALIGN (1<<4)
#define FLAG_SIGN       (1<<5)
#define FLAG_UPPER      (1<<6)
#define FLAG_PRECISION  (1<<7)

#define is_digit(c) ((c)>='0' && (c)<='9')

/* reads integer until first non-digit, incrementing the source pointer */
static unsigned int sm_atoi_skip(const char **str)
{
    unsigned int i=0;

    while(**str && is_digit(**str))
    {
#if SM_BIT_SHIFTS
        i=((i<<3)+(i<<1)) + *((*str)++)-'0';
#else
        i=(i*10) + *((*str)++)-'0';
#endif /* SM_BIT_SHIFTS */
    }

    return i;
}

/* returns the size of the string up to count */
static size_t sm_strnlen(const char *s, size_t count)
{
    size_t i;
    for(i=0; s[i]!='\0' && i<count; ++i) ;
    return i;
}

/* writes an unsigned number in a certain base to a string, but in reverse */
static unsigned int sm_nutoa(char *c, unsigned int n, unsigned long number, unsigned int base,
        unsigned int flags)
{
    unsigned int i;
#if SM_GT_HEX
    const char lower[]="0123456789abcdefghijklmnopqrstuvwxyz";
    const char upper[]="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
#else
    const char lower[]="0123456789abcdef";
    const char upper[]="0123456789ABCDEF";
#endif /* SM_GT_HEX */
    const char *digits;

    if(!n)
        return 0;

    if(flags & FLAG_UPPER)
        digits=upper;
    else
        digits=lower;

    i=0;
#if SM_BIT_SHIFTS
    if(base==8)
    {
        do
        {
            c[i++]=digits[number%base];
            number=number>>3;
        }
        while(number && i<(n-1));
    }
    else if(base==16)
    {
        do
        {
            c[i++]=digits[number%base];
            number=number>>4;
        }
        while(number && i<(n-1));
    }
#else
    do
    {
        c[i++]=digits[number%base];
        number/=base;
    }
    while(number && i<(n-1));
#endif /* SM_BIT_SHIFTS */
    c[i+1]='\0';
    return i;
}

int sm_vprintf(const char *format, va_list args, sm_putc_func sm_putc)
{
    int char_written=0;

    /* go through format string */
    for(; *format; ++format)
    {
        unsigned int flags=0, base=10;
        int width=-1, precision=-1, length=0;
        unsigned long number;

        /* if not percent */
        if(*format!='%')
        {
            /* write character */
            sm_putc(*format);
            ++char_written;
            continue;
        }

        /* flags */
        while(true)
        {
            /* removes first % and flags in the next switch statement */
            ++format;

            switch(*format)
            {
                case '#': flags|=FLAG_ALT_FORM; continue;
                case '0': flags|=FLAG_PAD_ZERO; continue;
                case ' ': flags|=FLAG_PAD_SPACE; continue;
                case '+': flags|=FLAG_INC_PLUS; continue;
                case '-': flags|=FLAG_LEFT_ALIGN; continue;
                default: break;
            }
            break;
        }

        /* field width */
        if(is_digit(*format))
            width=sm_atoi_skip(&format);
        else if(*format=='*') /* width provided as argument */
        {
            ++format;
            width=va_arg(args, int);
            if(width<0)
            {
                width=-width;
                flags|=FLAG_LEFT_ALIGN;
            }
        }

        /* precision */
        if(*format=='.')
        {
            ++format;
            if(is_digit(*format))
                precision=sm_atoi_skip(&format);
            else if(*format=='*')
            {
                ++format;
                precision=va_arg(args, int);
            }

            flags|=FLAG_PRECISION;
            flags&=~FLAG_PAD_ZERO;

            /* if only a . is given (i.e. 12.f), precision is taken to be zero
             * additionally, precision can only be positive, as we initialise it
             * to -1, we can just check if it is negative here
             */
            if(precision<0)
                precision=0;
        }

        /* length modifier */
        switch(*format)
        {
            case 'l':
            case 'h':
                length=*format;
                ++format;
                break;
            default:
                break;
        }

        /* conversion specifier & output */
        switch(*format)
        {
            /* signed int */
            case 'd':
            case 'i':
                flags|=FLAG_SIGN;
                break;
            /* unsigned int */
            case 'u':
                break;
            /* hexadecimal */
            case 'X':
                flags|=FLAG_UPPER;
                /* intentional fall-thru */
            case 'x':
                base=16;
                break;
            /* pointer */
            case 'p':
                /* pointer prints like %#x or %#lx */
                flags|=FLAG_ALT_FORM;
                /* set width if it hasn't been */
                if(width==-1)
                {
                    width=2*sizeof(void*);
                    flags|=FLAG_PAD_ZERO;
                }
                break;
#if SM_OCTAL
            /* octal */
            case 'o':
                base=8;
                break;
#endif /* SM_OCTAL */
#if SM_BINARY
            /* EXT: binary */
            case 'b':
                base=2;
                break;
#endif /* SM_BINARY */

            /* character */
            case 'c':
                /* left space pad */
                if(!(flags & FLAG_LEFT_ALIGN))
                {
                    while(width>0)
                    {
                        sm_putc(' ');
                        ++char_written;
                        --width;
                    }
                }
                /* write character */
                sm_putc((unsigned char) va_arg(args, int));
                ++char_written;
                /* padding, if required */
                while(width>0)
                {
                    sm_putc(' ');
                    ++char_written;
                    --width;
                }
                continue;
            /* string */
            case 's':
                {
                    int str_len, i;
                    char *str=va_arg(args, char*);
                    if(!str)
                        str="<null>";
                    str_len=sm_strnlen(str, precision);
                    /* left space pad */
                    if(!(flags & FLAG_LEFT_ALIGN))
                    {
                        while(width>0)
                        {
                            sm_putc(' ');
                            ++char_written;
                            --width;
                        }
                    }
                    /* string output */
                    for(i=0; i<str_len; ++i, ++char_written)
                        sm_putc(str[i]);
                    /* padding, if required */
                    while(width>0)
                    {
                        sm_putc(' ');
                        ++char_written;
                        --width;
                    }
                }
                continue;

#if SM_FLOAT
            /* float */
            case 'E':
            case 'F':
            case 'G':
                flags|=FLAG_UPPER;
            case 'e':
            case 'f':
            case 'g':
                /* output float */
                continue;
#endif /* SM_FLOAT */

#if SM_WRITTEN_SO_FAR
            /* number of characters so far written */
            case 'n':
                {
                    int *num_written=va_arg(args, int*);
                    *num_written=char_written;
                }
                continue;
#endif /* SM_WRITTEN_SO_FAR */

            /* no conversion, just % */
            case '%':
            default:
                sm_putc(*format);
                ++char_written;
                continue;
        }

        /* output integers */
        {
            /* number conversion */
            char buf[26];
            unsigned int number_len, is_neg=0;

            /* get number */
            if(length=='l')
                number=va_arg(args, unsigned long);
            else
                number=va_arg(args, unsigned int);
            /* deal with signs */
            if(flags & FLAG_SIGN)
            {
                if(length=='l')
                {
                    if((long)number<0)
                    {
                        is_neg=1;
                        number=-number;
                    }
                }
                else
                {
                    if((int)number<0)
                    {
                        is_neg=1;
                        number=-(int)number;
                    }
                }
            }
            else
            {
                flags&=~FLAG_INC_PLUS;
                flags&=~FLAG_PAD_SPACE;
            }

            /* convert number to string & get its length */
            number_len=sm_nutoa(buf, 26, number, base, flags);

            /* calculate length of formatted number */
            /* decrement for sign */
            if(is_neg || (flags & FLAG_INC_PLUS) || (flags & FLAG_PAD_SPACE))
                --width;
            /* subtract number length */
            width-=number_len;
            /* subtract precision */
            precision-=number_len;
            if(precision>0)
                width-=precision;
            /* subtract alt form */
            if(flags & FLAG_ALT_FORM)
            {
                switch(base)
                {
                    case 8: --width; break;
                    case 16: width-=2; break;
                    default: break;
                }
            }

            /* left space pad */
            if(!(flags & FLAG_LEFT_ALIGN) && !(flags & FLAG_PAD_ZERO))
                for(; width>0; --width, ++char_written)
                    sm_putc(' ');

            /* sign output */
            if(is_neg)
            {
                sm_putc('-');
                ++char_written;
            }
            else if(flags & FLAG_INC_PLUS)
            {
                sm_putc('+');
                ++char_written;
            }
            else if(flags & FLAG_PAD_SPACE)
            {
                sm_putc(' ');
                ++char_written;
            }

            /* alt form output */
            if(flags & FLAG_ALT_FORM)
            {
                switch(base)
                {
                    case 8:
                        sm_putc('0');
                        ++char_written;
                        break;
                    case 16:
                        sm_putc('0');
                        sm_putc((flags & FLAG_UPPER)?'X':'x');
                        char_written+=2;
                        break;
                    default:
                        break;
                }
            }

            /* zero pad */
            if(!(flags & FLAG_LEFT_ALIGN))
            {
                if(flags & FLAG_PRECISION)
                {
                    for(; width>0; --width, ++char_written)
                        sm_putc(' ');
                    for(; precision>0; --precision, ++char_written)
                        sm_putc('0');
                }
                else
                {
                    for(; width>0; --width, ++char_written)
                        sm_putc('0');
                }
            }

            /* number output */
            while(number_len)
            {
                sm_putc(buf[--number_len]);
                ++char_written;
            }

            /* right padding */
            for(; width>0; --width, ++char_written)
                sm_putc(' ');
        }

        /* check if we have just removed the last character from the format string */
        if(!*format)
            break;

        /* https://github.com/jpbonn/coremark_lm32/blob/master/ee_printf.c
         * http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf pg 309 (327)
         */
    }

    return char_written;
}

int sm_printf(const char *format, ...)
{
    va_list ap;
    int ret;

    va_start(ap, format);
    ret=sm_vprintf(format, ap, sm_write);
    va_end(ap);

    return ret;
}

#if SM_SNPRINTF
static char *__sm_str;
static size_t __sm_str_size;
static size_t __sm_str_count;

static void sm_str_write(int ch)
{
    if(__sm_str_count >= __sm_str_size)
        return;

    if(__sm_str_count == (__sm_str_size-1))
        __sm_str[__sm_str_count]='\0';
    else
        __sm_str[__sm_str_count]=ch;

    ++__sm_str_count;
}

int sm_snprintf(char *str, size_t size, const char *format, ...)
{
    va_list ap;
    int ret;

    __sm_str=str;
    __sm_str_size=size;
    __sm_str_count=0;

    va_start(ap, format);
    ret=sm_vprintf(format, ap, sm_str_write);
    va_end(ap);

    sm_str_write('\0');

    return ret;
}

#endif /* SM_SNPRINTF */

