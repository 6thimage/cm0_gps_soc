#! /usr/bin/env python

from struct import unpack

with open('image.bin', 'rb') as f:
    with open('image.vhex', 'w') as w:
        d = f.read(4)
        counter = 0
        while d:
            if not counter%16:
                w.write('// {:#010x}\n'.format(counter))
            w.write('{:08x}\n'.format(unpack("I",d)[0]))
            counter = counter+1
            d = f.read(4)
